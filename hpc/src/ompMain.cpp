#include "ompMain.h"
#include <cmath>
#include <iostream>
#include <chrono>
#include <vector>
#include <iomanip>
#include <thread>
#include <omp.h>

int iterations = 100;
const int gridDimension = 10000;
const int threadCount = 12;
const float heatFlowRate = 1.0f / 100;

bool isInsideCircle(float x, float y)
{
	return std::sqrt(x * x + y * y) < 1;
}

float someExpensiveFunction(float x)
{
	int expensiveness = 100;
	for (int i = 0; i < expensiveness; ++i)
	{
		x = sin(x);
	}
	return x;
}

inline float calculateHeatFlowAtLocation(std::vector<std::vector<float>>& grid, int x, int y, float rate)
{
	float result = grid[x][y];
	if (y >= 1)
	{
		result += (grid[x][y - 1] - grid[x][y]) * rate;
	}

	if (y < gridDimension - 1)
	{
		result += (grid[x][y + 1] - grid[x][y]) * rate;
	}

	if (x >= 1)
	{
		result += (grid[x - 1][y] - grid[x][y]) * rate;
	}

	if (x < gridDimension - 1)
	{
		result += (grid[x + 1][y] - grid[x][y]) * rate;
	}

	return result;
}

void print2dArray(std::vector<std::vector<float>>& grid)
{
	for (int x = 0; x < gridDimension; ++x)
	{
		for (int y = 0; y < gridDimension; ++y)
		{
			std::cout << std::setw(4) << std::setfill('0') << (int)grid[x][y] << "|";
		}
		std::cout << std::endl;
	}
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
}

std::vector<std::vector<float>> initArray()
{
	std::vector<std::vector<float>> grid;
	grid.resize(gridDimension);
	for (int j = 0; j < gridDimension; ++j)
	{
		grid[j].resize(gridDimension);
		for (int i = 0; i < gridDimension; ++i)
		{
			grid[j][i] = 0;
		}
	}
	return grid;
}

void runOmp()
{
	auto gridStep1 = initArray();
	auto gridStep2 = initArray();
	gridStep1[gridDimension / 2][gridDimension / 2] = 1000;
	const auto itemsPerThread = gridDimension / threadCount;
	while (iterations > 0)
	{
		auto begin = std::chrono::high_resolution_clock::now();
#pragma omp parallel num_threads(threadCount)
		{
			const auto threadNumber = omp_get_thread_num();
			int loopStart = itemsPerThread * threadNumber;
			auto loopEnd = itemsPerThread * (threadNumber + 1);
			for (int x = loopStart; x < loopEnd; x++)
			{
				for (int y = 0; y < gridDimension; y++)
				{
					gridStep2[x][y] = calculateHeatFlowAtLocation(gridStep1, x, y, heatFlowRate);
				}
			}
		}
		iterations--;
		std::swap(gridStep1, gridStep2);

		auto end = std::chrono::high_resolution_clock::now();
		auto dur = end - begin;
		auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
		std::cout << ms << std::endl;
		//std::cout << wongs << std::endl;
		//print2dArray(gridStep1);
	}
}
