from adafruit_servokit import ServoKit
import time

itemsCount = 12
FRL = 0
FRU = 1
FRH = 2

RRH = 3
RRU = 4
RRL = 5

FLH = 6
FLU = 7
FLL = 8

RLH = 9
RLU = 10
RLL = 11
kit = ServoKit(channels=16)
middlePoints = [110, 100, 65, 40, 90, 70, 110, 90, -1, 40, 0, -1]
currentPosition = middlePoints
targetLocations = [0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1]
speed = 50
updatesPerSecond = 100
lastTime = 0

targetLocations[FRL] = 999
targetLocations[RRL] = -999

targetLocations[FRU] = 90
targetLocations[RRU] = -90
targetLocations[FLU] = -0
targetLocations[RLU] = 0


def lerpPosition(slot):
    absoluteTargetPosition = middlePoints[slot] + targetLocations[slot]
    if absoluteTargetPosition < currentPosition[slot]:
        currentPosition[slot] -= speed / updatesPerSecond
    if absoluteTargetPosition > currentPosition[slot]:
        currentPosition[slot] += speed / updatesPerSecond

    if currentPosition[slot] > 180:
        currentPosition[slot] = 180
    if currentPosition[slot] < 0:
        currentPosition[slot] = 0


while True:
    if time.monotonic() > (lastTime + 1 / updatesPerSecond):
        lastTime = time.monotonic()
        for slot in range(0, itemsCount):
            lerpPosition(slot)
            kit.servo[slot].set_pulse_width_range(500, 2500)
            kit.servo[slot].actuation_range = 180
            kit.servo[slot].angle = currentPosition[slot]
