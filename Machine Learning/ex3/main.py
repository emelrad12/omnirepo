import pandas as pd
import tensorflow as tf
import numpy as np
from sklearn.model_selection import train_test_split

Sequential = tf.keras.models.Sequential
layers = tf.keras.layers
optimizers = tf.keras.optimizers
to_categorical = tf.keras.utils.to_categorical

allData = pd.read_csv("train.csv")
labelsTable = allData["label"].values
pixelTable = allData.drop("label", axis=1).values
pixelTable = pixelTable.reshape(-1, 28, 28, 1) / 255.0
labelsTable = to_categorical(labelsTable, num_classes=10)
trainPixels, testPixels, trainLabels, testLabels = train_test_split(pixelTable, labelsTable, test_size=0.50)


def build_model(pixels, labels):
    model = Sequential()
    model.add(layers.Flatten())
    model.add(layers.Dense(units=256, activation='tanh'))
    model.add(layers.Dense(units=256, activation='tanh'))
    model.add(layers.Dense(units=256, activation='tanh'))
    model.add(layers.Dense(units=256, activation='sigmoid'))
    model.add(layers.Dense(units=10, activation='softmax'))
    model.compile(optimizer="Nadam", loss='categorical_crossentropy', metrics=['accuracy'])
    model.fit(pixels, labels, epochs=10)
    return model


def getHighestPrediction(prediction):
    highest = 0
    highestIndex = 0
    index = 0
    for item in prediction:
        if item > highest:
            highestIndex = index
            highest = item
        index += 1
    return highestIndex


m = build_model(trainPixels, trainLabels)
pred = m.predict([testPixels])
getHighestPrediction(pred[0])
results = []
for item in pred:
    results.append(getHighestPrediction(item))
error = 0
for i in range(len(results)):
    error += np.sum(abs(testLabels[i] - pred[i]))
print(error/len(results))
