#pragma once
#include "Material.h"
#include "Vector3.h"

class Sphere
{
public:
	float radius;
	Material material;
	Vector3 position;
};
