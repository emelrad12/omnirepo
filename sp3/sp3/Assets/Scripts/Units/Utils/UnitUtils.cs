﻿using Units.Instances;
using UnityEngine;

namespace Units.Utils
{
    public static class UnitUtils
    {
        public static bool IsUnitInRange(Unit own, Unit target, float rangeMultiplier = 1)
        {
            Debug.Assert(target != null);
            Debug.Assert(own != null);
            return own.maxRange * rangeMultiplier > (target.Position - own.Position).magnitude;
        }

        public static float CalculateMaximalRange(Unit unit)
        {
            float maxRange = 0;
            foreach (var weapon in unit.weapons.Values)
            {
                if (weapon.template.range > maxRange)
                {
                    maxRange = weapon.template.range;
                }
            }

            return maxRange;
        }
    }
}