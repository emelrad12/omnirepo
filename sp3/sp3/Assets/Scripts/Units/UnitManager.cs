﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnitBehaviour;
using Units.Instances;
using UnityEngine;

namespace Units
{
    public class UnitManager
    {
        public Dictionary<Guid, Unit> units = new Dictionary<Guid, Unit>();
        public ConcurrentBag<Guid> deletionQueue = new ConcurrentBag<Guid>();
        public Pathfinder pathfinder = new Pathfinder();

        public void AddUnit(Unit unit)
        {
            units.Add(unit.guid, unit);
        }

        public void RemoveUnit(Unit unit)
        {
            deletionQueue.Add(unit.guid);
        }

        public void Update()
        {
            foreach (var item in deletionQueue)
            {
                units.Remove(item);
            }

            Parallel.ForEach(units.Values, (unit) => { unit.Update(); });
            foreach (var unit in units.Values)
            {
                unit.CopyDataToNative();
            }
        }

        public void SetAllUnitsTarget(Vector3 target)
        {
            foreach (var unit in units.Values)
            {
                unit.PathTo(target);
            }
        }
    }
}