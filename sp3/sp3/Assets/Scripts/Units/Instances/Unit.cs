﻿using System;
using System.Collections.Generic;
using System.Linq;
using Economy;
using ECS;
using Global.Templates;
using UnitBehaviour;
using Units.Factories;
using Units.Utils;
using UnityEngine;

namespace Units.Instances
{
    public class Unit
    {
        public static int idCounter;

        public Unit(UnitTemplate unitTemplate, Player owner, int slot)
        {
            this.slot = slot;
            template = unitTemplate;
            this.owner = owner;
            foreach (var templateWeapon in unitTemplate.weapons)
            {
                var weapon = WeaponFactory.InstantiateWeapon(templateWeapon, this);
                weapons.Add(weapon.guid, weapon);
            }

            maxRange = UnitUtils.CalculateMaximalRange(this);
            _path = Managers.unitManager.pathfinder.FindPathFromTo(Position, Position, template.movementTemplate.type);
        }

        public readonly int id = idCounter++;
        public readonly int slot;
        public readonly UnitTemplate template;
        public bool IsAlive => EcsManager.ecsUnitManager.unitNativeData.IsAlive(slot);
        public readonly Dictionary<Guid, Weapon> weapons = new Dictionary<Guid, Weapon>();
        public readonly Guid guid = Guid.NewGuid();
        public Unit target;
        public Player owner;
        public float maxRange;
        public bool stayStill;
        public float Health => EcsManager.ecsUnitManager.unitNativeData.health[slot];

        public Vector3 TargetLocation
        {
            get => EcsManager.ecsUnitManager.unitNativeData.targetLocation[slot];
            set => EcsManager.ecsUnitManager.unitNativeData.targetLocation[slot] = value;
        }

        public bool ShouldBroadside
        {
            get => EcsManager.ecsUnitManager.unitNativeData.shouldBroadside[slot];
            set => EcsManager.ecsUnitManager.unitNativeData.shouldBroadside[slot] = value;
        }

        public Vector3 Velocity => EcsManager.ecsUnitManager.unitNativeData.velocities[slot];
        public Vector3 Position => EcsManager.ecsUnitManager.unitNativeData.positions[slot];
        public Quaternion Rotation => EcsManager.ecsUnitManager.unitNativeData.rotations[slot];

        public void TargetEnemy(Unit unit)
        {
            Debug.Assert(unit != null);

            if (unit.owner == owner)
            {
                throw new Exception("Cannot target own faction");
            }

            target = unit;
            foreach (var weapon in weapons.Values)
            {
                weapon.Target(target);
            }
        }

        public void Destroy()
        {
            Managers.unitManager.RemoveUnit(this);
        }

        public void PathTo(Vector3 location)
        {
            if ((_path.waypoints.Last() - location).magnitude > 5)
            {
                RecalculatePath(TargetLocation);
            }

            RecalculatePath(location);
        }

        public void Update()
        {
            if (Health <= 0)
            {
                Destroy();
                return;
            }

            foreach (var weapon in weapons.Values) weapon.Update();
        }

        public void CopyDataToNative()
        {
            EcsManager.ecsUnitManager.unitNativeData.stayStill[slot] = stayStill;
            TargetLocation = _path.GetWaypoint(Position);
            foreach (var weapon in weapons.Values)
            {
                weapon.CopyDataToNative();
            }
        }

        private MovementPath _path;

        private void RecalculatePath(Vector3 targetPositon)
        {
            _path = Managers.unitManager.pathfinder.FindPathFromTo(Position, targetPositon, template.movementTemplate.type);
        }
    }
}