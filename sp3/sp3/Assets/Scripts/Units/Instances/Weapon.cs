﻿using System;
using ECS;
using Global;
using Global.Templates;
using Units.Factories;
using UnityEngine;

namespace Units.Instances
{
    public class Weapon
    {
        public Weapon(WeaponTemplate template, Unit unit)
        {
            parent = unit;
            this.template = template;
            switch (template.type)
            {
                case WeaponType.Projectile:
                    ShellData = new ShellData();
                    ShellData.material = ModelFactory.LoadMaterial("Laser");
                    ShellData.mesh = ModelFactory.LoadShellMesh();
                    break;
                case WeaponType.Beam:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public int cooldown;
        public WeaponTemplate template;
        public Guid guid = Guid.NewGuid();
        public Unit parent;
        public ShellData ShellData;
        public int slot;
        public Vector3 CurrentPosition => parent.Position + (ParentRotation * OffsetFromUnitCenter);

        public Vector3 OffsetFromUnitCenter => template.offset;
        public Quaternion ParentRotation => parent.Rotation;

        public void Target(Unit unit)
        {
            _target = unit;
        }

        public void Update()
        {
            cooldown--;

            if (_target == null || !_target.IsAlive)
            {
                _target = null;
                return;
            }

            if (cooldown <= 0)
            {
                if (EcsManager.ecsWeaponManager.weaponNativeData.currentAngleToTarget[slot] < 2)
                {
                    FireAtTarget();
                }
            }
        }

        public void FireAtTarget()
        {
            cooldown = 60 * (int) (1 / template.fireRate);
            switch (template.type)
            {
                case WeaponType.Projectile:
                    FireShell(_target);
                    break;
                case WeaponType.Beam:
                    FireLaser(_target);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void FireLaser(Unit unit)
        {
        }

        private void FireShell(Unit unit)
        {
            var shell = EcsManager.ecsWeaponManager.projectileManager.GetNewProjectile();
            shell.mesh = ShellData.mesh;
            shell.material = ShellData.material;
            shell.ecsProjectileData.drop = template.dropOff;
            shell.ecsProjectileData.parentSlot = slot;
            shell.ecsProjectileData.position = EcsManager.ecsWeaponManager.weaponNativeData.barrelOutputPosition[slot];
            shell.ecsProjectileData.homing = template.homing;
            shell.ecsProjectileData.allegiance = parent.owner.id;
            shell.ecsProjectileData.size = 0.5f;
            shell.ecsProjectileData.damage = template.damage;
            shell.ecsProjectileData.speed = template.speed;
            shell.ecsProjectileData.turnRate = 5;
            shell.ecsProjectileData.targetSlot = unit.slot;
            shell.ecsProjectileData.lifespan = 3;
            shell.ecsProjectileData.rotation = Globals.RandomizeQuaternion(EcsManager.ecsWeaponManager.weaponNativeData.barrelRotations[slot], template.inaccuracy);
            EcsManager.ecsWeaponManager.projectileManager.AddProjectile(shell);
        }

        private Unit _target;

        public void CopyDataToNative()
        {
            if (parent.target != null)
            {
                EcsManager.ecsWeaponManager.weaponNativeData.targetedUnitSlot[slot] = parent.target.slot;
            }
        }
    }

    public class ShellData
    {
        public Mesh mesh;
        public Material material;
    }
}