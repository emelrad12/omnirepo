﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Economy;
using Newtonsoft.Json;
using Units.Instances;
using UnityEngine;
using ECS;
using ECS.Managers;
using Global;
using Global.Templates;

namespace Units.Factories
{
    public static class UnitFactory
    {
        public static Dictionary<string, UnitTemplate> templates;

        public static void LoadUnitTemplates()
        {
            templates = new Dictionary<string, UnitTemplate>();
            string[] subDirectories = Directory.GetDirectories("Assets/Resources/UnitList");
            foreach (var directory in subDirectories)
            {
                var directoryName = directory.Split('\\').Last();
                if (directoryName == "ToFix")
                {
                    continue;
                }

                Debug.Log("Loading " + directoryName);
                var unitData = File.ReadAllText("Assets/Resources/UnitList/" + directoryName + "/UnitData.json");
                var unit = JsonConvert.DeserializeObject<UnitTemplate>(unitData, Globals.jsonSettings);
                templates.Add(unit.name, unit);
            }
        }

        public static Unit Instantiate(UnitTemplate unitTemplate, Vector3 position, Player owner)
        {
            EcsUnit ecsUnitData = EcsManager.ecsUnitManager.GetEcsUnit();
            ecsUnitData.position = position;
            ecsUnitData.additionalUnitData.acceleration = unitTemplate.movementTemplate.acceleration;
            ecsUnitData.rotation = Quaternion.identity;
            ecsUnitData.additionalUnitData.allegiance = owner.id;
            ecsUnitData.additionalUnitData.maxSpeed = unitTemplate.movementTemplate.speed;
            ecsUnitData.additionalUnitData.turnRate = unitTemplate.movementTemplate.turnSpeed;
            ecsUnitData.additionalUnitData.type = unitTemplate.movementTemplate.type;
            ecsUnitData.additionalUnitData.broadside = unitTemplate.preferBroadside;
            ecsUnitData.material = ModelFactory.LoadMaterial();
            ecsUnitData.mesh = ModelFactory.LoadUnitMesh(unitTemplate.name);
            ecsUnitData.collider = ModelFactory.GetConvexCollider(unitTemplate.name);
            ecsUnitData.health = unitTemplate.maxHealth;
            EcsManager.ecsUnitManager.SpawnUnit(ecsUnitData);
            Unit unit = new Unit(unitTemplate, owner, ecsUnitData.slot);
            owner.units.Add(unit.guid, unit);
            Managers.unitManager.AddUnit(unit);
            Managers.unitAiManager.CreateAi(unit);
            return unit;
        }
    }
}