﻿using System;
using ECS;
using ECS.Jobs;
using ECS.Managers;
using Global.Templates;
using Units.Instances;
using UnityEngine;

namespace Units.Factories
{
    public static class WeaponFactory
    {
        public static Weapon InstantiateWeapon(WeaponTemplate weaponTemplate, Unit unit)
        {
            var weapon = new Weapon(weaponTemplate, unit);
            var ecsWeapon = EcsManager.ecsWeaponManager.GetNewWeapon();
            weapon.slot = ecsWeapon.slot;
            ecsWeapon.hasTurret = weaponTemplate.hasTurret;
            ecsWeapon.offset = weaponTemplate.offset;
            ecsWeapon.defaultRotation = weaponTemplate.rotation;
            ecsWeapon.shellSpeed = weaponTemplate.speed;
            ecsWeapon.homing = weaponTemplate.homing;
            ecsWeapon.drop = weaponTemplate.dropOff;
            ecsWeapon.unitSlot = unit.slot;
            ecsWeapon.range = weaponTemplate.range;
            ecsWeapon.xLimits = weaponTemplate.maxX;
            ecsWeapon.yLimits = weaponTemplate.maxY;
            if (weapon.template.hasTurret)
            {
                var turretAndBarrelMesh = ModelFactory.LoadTurretAndBarrelMesh(unit.template.name, weaponTemplate.turretTemplate.name);
                ecsWeapon.turret = new EcsTurretManager.EcsTurret
                {
                    mesh = turretAndBarrelMesh[0],
                    material = ModelFactory.LoadMaterial(),
                    ecsTurretData = new EcsTurretManager.EcsTurretData
                    {
                        rotationSpeed = weaponTemplate.turretTemplate.turnRate,
                        barrelOffset = weaponTemplate.turretTemplate.barrelPosition,
                        barrelLength = weaponTemplate.turretTemplate.barrelLength
                    },
                    ecsBarrel = new EcsTurretManager.EcsBarrel
                    {
                        mesh = turretAndBarrelMesh[1],
                        material = ModelFactory.LoadMaterial(),
                    }
                };
            }
            
            EcsManager.ecsWeaponManager.AddWeapon(ecsWeapon);
            return weapon;
        }
    }
}