﻿using System.Collections.Generic;
using Global;
using Global.Other;
using Unity.Entities;
using Unity.Physics;
using UnityEditor.PackageManager;
using UnityEngine;
using Collider = Unity.Physics.Collider;
using Material = UnityEngine.Material;

namespace Units.Factories
{
    public static class ModelFactory
    {
        public static Dictionary<string, Mesh> unitMeshCache = new Dictionary<string, Mesh>();
        public static Dictionary<string, BlobAssetReference<Collider>> unitColliderCache = new Dictionary<string, BlobAssetReference<Collider>>();

        public static Mesh LoadUnitMesh(string name)
        {
            var gameObject = Resources.Load<GameObject>("UnitList/" + name + "/Model");
            var mesh = gameObject.GetComponent<MeshFilter>().sharedMesh;
            return mesh;
        }

        public static Mesh LoadShellMesh(string name = "DefaultShell")
        {
            if (unitMeshCache.ContainsKey(name)) return unitMeshCache[name];
            var mesh = Resources.Load<GameObject>("Projectiles/" + name + "/Model").GetComponent<MeshFilter>().sharedMesh;
            unitMeshCache[name] = mesh;
            return mesh;
        }

        public static BlobAssetReference<Collider> GetConvexCollider(string name)
        {
            if (unitColliderCache.ContainsKey(name)) return unitColliderCache[name];
            var mesh = LoadUnitMesh(name);
            var material = Unity.Physics.Material.Default;
            material.Flags = Unity.Physics.Material.MaterialFlags.EnableCollisionEvents |
                             Unity.Physics.Material.MaterialFlags.EnableMassFactors |
                             Unity.Physics.Material.MaterialFlags.EnableSurfaceVelocity;
            var collider = ConvexCollider.Create(
                Transformers.CopyToNativeArray(mesh.vertices),
                ConvexHullGenerationParameters.Default,
                CollisionFilter.Default,
                material);
            unitColliderCache[name] = collider;
            return collider;
        }

        public static List<Mesh> LoadTurretAndBarrelMesh(string unitName, string turretName)
        {
            var turretMesh = Resources.Load<GameObject>("UnitList/" + unitName + "/Turrets/" + turretName + "/Model").GetComponent<MeshFilter>().sharedMesh;
            var barrelMesh = Resources.Load<GameObject>("UnitList/" + unitName + "/Turrets/" + turretName + "/Barrel").GetComponent<MeshFilter>().sharedMesh;
            return new List<Mesh>
            {
                turretMesh,
                barrelMesh
            };
        }

        public static Material LoadMaterial(string name = "Default")
        {
            var material = Resources.Load<Material>("Materials/" + name + "/Material"); //todo
            return material;
        }
    }
}