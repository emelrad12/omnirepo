﻿using System.Collections.Generic;
using Global.Templates;
using UnityEngine;

namespace Units.Factories
{
    public class RandomFactories
    {
        public static List<WeaponTemplate> GetRandomWeapons()
        {
            var weapons = new List<WeaponTemplate>();
            for (int i = 0; i < 3; i++)
            {
                var templateParams = new WeaponTemplate();
                templateParams.damage = 1;
                templateParams.range = 10;
                templateParams.type = WeaponType.Beam;
                templateParams.fireRate = 1;
                templateParams.name = "Laser";
                templateParams.offset = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                templateParams.maxX = new Vector2(-180, 180);
                templateParams.maxY = new Vector2(-180, 180);
                templateParams.turretTemplate = new TurretTemplate
                {
                    turnRate = 1,

                    name = "Main"
                };
                weapons.Add(templateParams);
            }

            return weapons;
        }

        public static MovementTemplate GetRandomMovement()
        {
            return new MovementTemplate();
        }

        public static UnitTemplate GetRandomUnit()
        {
            var unit = new UnitTemplate
            {
                weapons = GetRandomWeapons(), 
                movementTemplate = GetRandomMovement()
            };
            unit.name = "Default";
            return unit;
        }
    }
}