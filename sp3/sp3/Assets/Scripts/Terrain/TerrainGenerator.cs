﻿using System;
using System.Collections.Generic;
using ECS;
using Units.Factories;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Terrain
{
    public class TerrainGenerator
    {
        public float scale = 10;
        public NativeArray<float> heights;

        public float EditHeight(float height)
        {
            var maxHeight = -20;
            var minHeight = -20;
            height *= 2;
            return math.clamp(height, minHeight, maxHeight);
        }

        public void GenerateRandomTerrain(int width, int length)
        {
            var vertices = new Vector3[(width + 1) * (length + 1)];
            heights = new NativeArray<float>((width + 1) * (length + 1), Allocator.Persistent);
            for (int z = 0, i = 0; z <= length; z++)
            {
                for (int x = 0; x <= width; x++)
                {
                    var height = GetHeight(x, z);
                    height = EditHeight(height);
                    heights[i] = height;
                    vertices[i] = new Vector3(x * scale, height, z * scale);
                    i++;
                }
            }

            int vert = 0;
            int tris = 0;
            var triangles = new int[6 * width * length];
            for (int z = 0; z < length; z++)
            {
                for (int x = 0; x < width; x++)
                {
                    triangles[tris + 0] = vert + 0;
                    triangles[tris + 1] = vert + width + 1;
                    triangles[tris + 2] = vert + 1;
                    triangles[tris + 3] = vert + 1;
                    triangles[tris + 4] = vert + width + 1;
                    triangles[tris + 5] = vert + width + 2;
                    vert++;
                    tris += 6;
                }

                vert++;
            }

            EcsManager.ecsTerrainGenerator.GenerateTerrain(triangles, vertices, ModelFactory.LoadMaterial("Ground"));
        }

        private readonly List<Vector3> noisesValues = new List<Vector3>()
        {
            new Vector3(1 / 200f, 1 / 200f, 400),
            new Vector3(1 / 50f, 1 / 50f, 100),
            new Vector3(1 / 15f, 1 / 15f, 15)
        };

        private float GetHeight(float x, float z)
        {
            float finalValue = 0;
            foreach (var item in noisesValues)
            {
                finalValue += Mathf.PerlinNoise((x + 0.1f) * item.x, (z + 0.1f) * item.y) * item.z;
            }

            return finalValue - 250;
        }
    }
}