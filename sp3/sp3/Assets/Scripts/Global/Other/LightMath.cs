﻿using Unity.Mathematics;
using UnityEngine;

namespace Global.Other
{
    public static class LightMath
    {
        public static Vector2 LerpVector2(Vector2 oldFloat, Vector2 newFloat, float amount)
        {
            var xDiff = Mathf.Abs(amount / (oldFloat.x - newFloat.x));
            var yDiff = Mathf.Abs(amount / (oldFloat.y - newFloat.y));
            return new Vector2
            {
                x = Mathf.Lerp(oldFloat.x, newFloat.x, xDiff),
                y = Mathf.Lerp(oldFloat.y, newFloat.y, yDiff)
            };
        }
    }
}