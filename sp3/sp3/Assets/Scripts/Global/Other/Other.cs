﻿using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Profiling;

namespace Global.Other
{
    public static class Transformers
    {
        public static NativeArray<float3> CopyToNativeArray(Vector3[] items)
        {
            Profiler.BeginSample("Custom Sample");
            var returnValue = new NativeArray<float3>(items.Length, Allocator.Persistent);
            for (int i = 0; i < items.Length; i++)
            {
                returnValue[i] = items[i];
            }

            Profiler.EndSample();
            return returnValue;
        }

        public static NativeArray<int3> ConvertToInt3Array(int[] items)
        {
            var returnValue = new NativeArray<int3>(items.Length / 3, Allocator.Persistent);
            for (int i = 0; i < items.Length / 3; i++)
            {
                returnValue[i] = new int3(items[i * 3 + 0], items[i * 3 + 1], items[i * 3 + 2]);
            }

            return returnValue;
        }
    }

    public static class Randoms
    {
        public static System.Random random = new System.Random();

        public static Vector3 RandomVector3(float range)
        {
            return new Vector3((float) random.NextDouble() * range, (float) random.NextDouble() * range, (float) random.NextDouble() * range);
        }

        public static Vector3 RandomVector3Flat(float range)
        {
            return new Vector3((float) random.NextDouble() * range, 0, (float) random.NextDouble() * range);
        }
    }

    public static class HeavyMath
    {
        public static float AngleToTargetOnRotatedPlane(Quaternion rotation, Vector3 relativePosition, bool up)
        {
            var positionInLocal = Quaternion.Inverse(rotation) * relativePosition;
            if (up)
            {
                positionInLocal.x = 0;
            }
            else
            {
                positionInLocal.y = 0;
            }

            var returnValue = AngleBetweenVectors(positionInLocal.normalized, Vector3.forward, up);
            return returnValue;
        }

        public static float AngleBetweenVectors(Vector3 vector1, Vector3 vector2, bool up)
        {
            var cross = Vector3.Cross(vector1, vector2);
            int sign = (up ? cross.x : cross.y) > 0 ? -1 : 1;
            return sign * Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(vector1, vector2) / (vector1.magnitude * vector2.magnitude));
        }

        public static Vector2 AngleToFireShell(float speed, float distance, float heightDifference)
        {
            var speedSqr = speed * speed;
            var innerMath = Mathf.Sqrt(speedSqr * speedSqr - GlobalVariables.G * (GlobalVariables.G * distance * distance + 2 * heightDifference * speedSqr));
            return new Vector2(
                Mathf.Rad2Deg * Mathf.Atan((speedSqr + innerMath) / (GlobalVariables.G * distance)),
                Mathf.Rad2Deg * Mathf.Atan((speedSqr - innerMath) / (GlobalVariables.G * distance))
            );
        }

        public static Vector3 ExtendUpwardsByAngle(float angle, Vector3 starting, Vector3 toExtend)
        {
            var yDiff = starting.y - toExtend.y;
            if (yDiff < 0)
            {
                yDiff = 0;
            }

            starting.y = 0;
            toExtend.y = 0;
            var distance = (toExtend - starting).magnitude;
            float y = distance * Mathf.Tan(Mathf.Deg2Rad * angle);
            toExtend.y = yDiff + y;
            return toExtend;
        }
    }
}