﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Global.JsonConverters
{
    public class Vector2Converter : JsonConverter
    {
        struct SVector2
        {
            public float x;
            public float y;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (!(value is Vector2 data))
            {
                throw new Exception("Failed to convert vector3");
            }

            serializer.Serialize(writer, new SVector2
            {
                x = data.x,
                y = data.y,
            });
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new Exception();
        }

        public override bool CanRead => false;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Vector2);
        }
    }
}