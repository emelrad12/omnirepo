﻿using System;
using System.Collections.Concurrent;

namespace Global
{ 
    public class FreeArraySpaceTracker
    {
        public FreeArraySpaceTracker(int size)
        {
            _positions = new ConcurrentStack<int>();
            for (int i = size - 1; i >= 0; i--)
            {
                _positions.Push(i);
            }

            this.size = size;
        }

        public int size;

        public int UsedSpace => size - _positions.Count;

        public int GetSpace()
        {
            if (!_positions.TryPop(out int result))
            {
                throw new Exception("Could not get space, increase space?");
            }

            return result;
        }

        public void FreeSpace(int space)
        {
            _positions.Push(space);
        }

        private readonly ConcurrentStack<int> _positions;
    }
}