﻿using UnityEngine;

namespace Global
{
    public class GameTimer
    {
        public int ticks;
        public float lastTime;
        public int elapsedTicks;

        public GameTimer(int maxTicks)
        {
            Set(maxTicks);
        }

        public void Set(int maxTicks)
        {
            ticks = maxTicks;
        }

        public void DecrementTimer()
        {
            ticks--;
            elapsedTicks++;
            if (ticks < 0 && ticks >= -1)
            {
                Debug.Log("Counted down for :" + Time.time);
            }

            if (Time.time - lastTime > 1)
            {
                lastTime = Time.time;
                Debug.Log("ticks in second: " + elapsedTicks);
                elapsedTicks = 0;
            }
        }
    }
}