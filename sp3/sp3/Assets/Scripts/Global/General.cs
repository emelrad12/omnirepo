﻿using Newtonsoft.Json;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Profiling;

namespace Global
{
    public static class Globals
    {
        static Globals()
        {
            jsonSettings = new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Error};
            random = new Unity.Mathematics.Random(0x6E624EB7u);
        }

        public static JsonSerializerSettings jsonSettings;
        public static Unity.Mathematics.Random random;

        public static float ClampsLaserWidth(float width, Vector3 position)
        {
            float minRatio = 625;
            return Mathf.Clamp(width, GetCameraDistance(position) / minRatio, width);
        }

        public static float GetCameraDistance(Vector3 positon)
        {
            return (GetCameraPosition() - positon).magnitude;
        }

        public static Vector3 GetCameraPosition()
        {
            return GetCamera().transform.position;
        }

        public static Camera GetCamera()
        {
            if (_mainCamera == null)
            {
                _mainCamera = Camera.main;
            }

            return _mainCamera;
        }

        public static Quaternion RandomizeQuaternion(Quaternion item, float randomization)
        {
            return new Quaternion(
                random.NextFloat(item.x - item.x * randomization, item.x + item.x * randomization),
                random.NextFloat(item.y - item.y * randomization, item.y + item.y * randomization),
                random.NextFloat(item.z - item.z * randomization, item.z + item.z * randomization),
                random.NextFloat(item.w - item.w * randomization, item.w + item.w * randomization));
        }

        public static bool IsInBounds(Vector3 position, Vector3 min, Vector3 max)
        {
            return position.x >= min.x && position.y >= min.y && position.z >= min.z && position.x <= max.x && position.y <= max.y && position.z <= max.z;
        }

        private static Camera _mainCamera;
    }
}