﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Unity.Mathematics;

namespace Global.Templates
{
    public struct UnitTemplate
    {
        [JsonProperty(Required = Required.Always)]
        public List<WeaponTemplate> weapons;
        [JsonProperty(Required = Required.Always)]
        public MovementTemplate movementTemplate;
        [JsonProperty(Required = Required.Always)]
        public string name;
        [JsonProperty(Required = Required.Always)]
        public bool preferBroadside;
        [JsonProperty(Required = Required.Always)]
        public float maxHealth;
    }
}