﻿using Global.JsonConverters;
using Newtonsoft.Json;
using UnityEngine;

namespace Global.Templates
{
    public class TurretTemplate
    {
        public float turnRate;
        [JsonConverter(typeof(Vector3Converter))]
        public Vector3 barrelPosition;
        public float barrelLength;
        public string name;
    }
}