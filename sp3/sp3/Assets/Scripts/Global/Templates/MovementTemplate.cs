﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using UnityEngine;

namespace Global.Templates
{
    public struct MovementTemplate
    {
        public enum MovementType
        {
            Land,
            Air,
            Gunship,
            Naval,
            Amphibious,
            Hover,
            None
        }

        public float speed;
        public float turnSpeed;
        public float acceleration;
        public float SpeedPerTick => speed / 60;
        public float TurnSpeedPerTick => turnSpeed / 60;

        [JsonConverter(typeof(StringEnumConverter))]
        public MovementType type;
    }
}