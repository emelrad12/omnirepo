﻿using System.Threading;
using Global.JsonConverters;
using Newtonsoft.Json;
using UnityEngine;

namespace Global.Templates
{
    public enum WeaponType
    {
        Projectile,
        Beam
    }

    public struct WeaponTemplate
    {
        [JsonProperty(Required = Required.Always)]
        public WeaponType type;
        [JsonProperty(Required = Required.Always)]
        public float range; 
        [JsonProperty(Required = Required.Always)]
        public float inaccuracy;
        [JsonProperty(Required = Required.Always)]
        public float damage;
        [JsonProperty(Required = Required.Always)]
        public Quaternion rotation;
        [JsonProperty(Required = Required.Always)]
        public float speed;
        [JsonProperty(Required = Required.Always)]
        public float fireRate;
        [JsonProperty(Required = Required.Always)]
        public string name;
        [JsonProperty(Required = Required.Always)]
        public bool hasTurret;
        [JsonProperty(Required = Required.Always)]
        public bool homing;
        [JsonProperty(Required = Required.Always)]
        public bool dropOff;
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(Vector2Converter))]
        public Vector2 maxY;
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(Vector2Converter))]
        public Vector2 maxX;
        [JsonProperty(Required = Required.Always)]
        [JsonConverter(typeof(Vector3Converter))]
        public Vector3 offset;
        //turret not always required
        public TurretTemplate turretTemplate;
    }
}