﻿using Economy;
using ECS;
using ECS.Jobs.Datas;
using GameAI;
using UnitBehaviour;
using UnitBehaviour.Targeting;
using Units;
using Unity.Mathematics;

public static class Managers
{
    public static void Instantiate()
    {
        unitManager = new UnitManager();
        targetManager = new TargetManager();
        playerManager = new PlayerManager();
        unitAiManager = new UnitAiManager();
        unitGrid = new UnitGrid(new int2
        {
            x = 100,
            y = 100
        }, 20);
       EcsManager.Init(100000);
    }

    public static UnitGrid unitGrid;
    public static TargetManager targetManager;
    public static UnitManager unitManager;
    public static PlayerManager playerManager;
    public static UnitAiManager unitAiManager;
    
    public static void Tick()
    {
        unitAiManager.UpdateAi();
        unitManager.Update();
        playerManager.ComputeResources();
    }
}