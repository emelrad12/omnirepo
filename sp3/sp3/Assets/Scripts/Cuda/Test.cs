﻿using System.Runtime.InteropServices;
using Debug = UnityEngine.Debug;

namespace Cuda
{
    public static class Cuda
    {
        private const string dllName = @"Other\CudaDll\x64\Dll\CudaDll.dll";
        [DllImport(dllName, CharSet = CharSet.Auto)]
        private static extern unsafe float* CudaTest();

        [DllImport(dllName, CharSet = CharSet.Auto)]
        private static extern unsafe float CudaGenerateNavMesh(float* heights, int sizeX, int sizeY);

        public static unsafe void Test()
        {
            CudaTest();
        }

        public static unsafe void GenerateNavMesh(float* heights, int sizeX, int sizeY)
        {
            // Debug.Log(CudaGenerateNavMesh(heights, sizeX, sizeY));
        }
    }
}