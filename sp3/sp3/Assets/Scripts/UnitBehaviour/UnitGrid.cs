﻿using System;
using System.Collections.Generic;
using Economy;
using Global;
using Units.Instances;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

namespace UnitBehaviour
{
    public class UnitGrid
    {
        public UnitGrid(int2 nodesCount, float nodeSize)
        {
            nodes = new List<List<Node>>(nodesCount.x);
            for (int x = 0; x < nodesCount.x; x++)
            {
                nodes.Add(new List<Node>(nodesCount.y));
                for (int y = 0; y < nodesCount.y; y++)
                {
                    nodes[x].Add(new Node(CalculateNodePosition(x, y)));
                }
            }

            maxBounds.x = nodeSize * nodesCount.x;
            maxBounds.z = nodeSize * nodesCount.y;
            maxBounds.y = 1000;
            minBounds.x = 0;
            minBounds.z = 0;
            minBounds.y = -100;
        }

        public void AddUnit(Unit unit)
        {
            if (unitPositionInGrid.ContainsKey(unit.guid))
            {
                var position = unitPositionInGrid[unit.guid];
                nodes[position.x][position.y].units[unit.owner].Remove(unit.guid); //todo too slow
            }

            var gridPosition = CalculateUnitPosition(unit);
            unitPositionInGrid[unit.guid] = gridPosition;
        }

        public bool IsInBounds(Vector3 position)
        {
            return Globals.IsInBounds(position, minBounds, maxBounds);
        }

        private int2 CalculateUnitPosition(Unit unit)
        {
            Vector3 position = unit.Position;
            return new int2
            {
                x = (int) ((position.x + 0.5f * nodeSize) / nodeSize)
            };
        }

        private Vector2 CalculateNodePosition(int x, int y)
        {
            return new Vector2((x + 0.5f) * nodeSize, (y + 0.5f) * nodeSize);
        }

        public Vector3 minBounds = Vector3.zero;
        public Vector3 maxBounds = Vector3.zero;
        public float nodeSize;
        public int2 nodeCount;
        public Dictionary<Guid, int2> unitPositionInGrid = new Dictionary<Guid, int2>();
        private List<List<Node>> nodes;

        public class Node
        {
            public Dictionary<Player, Dictionary<Guid, Unit>> units = new Dictionary<Player, Dictionary<Guid, Unit>>();
            public Vector2 center;

            public Node(Vector2 position)
            {
                center = position;
            }
        }
    }
}