﻿using System.Linq;
using Global.Other;
using Units.Instances;
namespace UnitBehaviour.Targeting
{
    public class TargetManager
    {
        public Unit GetBestTarget(Unit attacker)
        {
            return GetFirstTarget(attacker);
        }

        public Unit GetFirstTarget(Unit attacker)
        {
            foreach (var unit in Managers.unitManager.units.Values)
            {
                var randomUnit = Managers.unitManager.units.Values.ElementAt(Randoms.random.Next(Managers.unitManager.units.Count));//todo
                if (randomUnit.owner != attacker.owner && randomUnit.IsAlive)
                {
                    return randomUnit;
                }
            }

            return null;
        }
    }
}