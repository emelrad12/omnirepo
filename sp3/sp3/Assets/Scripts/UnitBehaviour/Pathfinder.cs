﻿using System.Collections.Generic;
using System.Linq;
using Global.Templates;
using UnityEngine;

namespace UnitBehaviour
{
    public class Pathfinder
    {
        public MovementPath FindPathFromTo(Vector3 from, Vector3 to, MovementTemplate.MovementType movementType, float atWaypointRange = 10)
        {
            var waypoints = new List<Vector3>();
            waypoints.Add(to);
            return new MovementPath(waypoints, atWaypointRange);
        }
    }

    public class MovementPath
    {
        public int currentWaypoint;
        public List<Vector3> waypoints;
        public float atWaypointRange;
        public bool isValid = true;
        public Vector3 end;

        public bool IsPathValidFor(Vector3 targetPosition)
        {
            return IsWholeComplete(targetPosition);
        }

        public bool IsComplete(Vector3 currentPosition)
        {
            return (currentPosition - waypoints[currentWaypoint]).magnitude < atWaypointRange;
        }

        public bool IsWholeComplete(Vector3 currentPosition)
        {
            if (!isValid)
            {
                return true;
            }
            var isComplete = (currentPosition - waypoints.Last()).magnitude < atWaypointRange;
            if (isComplete)
            {
                isValid = false;
            }
            
            return isComplete;
        }

        public MovementPath(List<Vector3> waypoints, float atWaypointRange)
        {
            this.waypoints = waypoints;
            this.atWaypointRange = atWaypointRange;
            end = waypoints.Last();
        }

        public Vector3 GetWaypoint(Vector3 currentPosition)
        {
            if (IsComplete(currentPosition))
            {
                if (currentWaypoint < waypoints.Count - 1) currentWaypoint++;
                return waypoints[currentWaypoint];
            }

            return waypoints[currentWaypoint];
        }
    }
}