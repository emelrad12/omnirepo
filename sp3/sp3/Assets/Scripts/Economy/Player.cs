﻿using System;
using System.Collections.Generic;
using Units.Instances;
using UnityEditor;

namespace Economy
{
    public class Player
    {
        public static int idCounter; 
        public Player(string name)
        {
            this.name = name;
        }

        public int id = idCounter++;
        public string name;
        public Guid guid = Guid.NewGuid();
        public string race;
        public float mass;
        public float energy;
        public float rares;

        public Dictionary<Guid, ResourceGenerator> resourceGenerators = new Dictionary<Guid, ResourceGenerator>();
        public Dictionary<Guid, Unit> units = new Dictionary<Guid, Unit>();
        public void ComputeResources()
        {
            foreach (var resourceGenerator in resourceGenerators.Values)
            {
                mass += resourceGenerator.template.massGenerated;
                energy += resourceGenerator.template.energyGenerated;
                rares += resourceGenerator.template.raresGenerated;
            }
        }
    }
}