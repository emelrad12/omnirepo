﻿using System;
using UnityEditor;
namespace Economy
{
    public class ResourceGenerator
    {
        public Guid guid = Guid.NewGuid();
        public ResourceGeneratorTemplate template;
        public ResourceGenerator(ResourceGeneratorTemplate resourceGeneratorTemplate)
        {
            template = resourceGeneratorTemplate;
        }
    }

    public enum ResourceType
    {
        Mass,
        Energy,
        Rares
    }

    public class ResourceGeneratorTemplate
    {
        public float massGenerated;
        public float energyGenerated;
        public float raresGenerated;
    }
}