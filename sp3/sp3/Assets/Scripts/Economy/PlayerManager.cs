﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;

namespace Economy
{
    public class PlayerManager
    {
        public Dictionary<Guid, Player> players = new Dictionary<Guid, Player>();

        public void AddPlayer(string name, string race)
        {
        }

        public void ComputeResources()
        {
            Parallel.ForEach(players.Values, (item) => item.ComputeResources());
        }

        public void AddPassiveResource()
        {
            foreach (var player in players.Values)
            {
                var template = new ResourceGeneratorTemplate
                {
                    massGenerated = 5,
                    energyGenerated = 50,
                    raresGenerated = 0.5f
                };
                var generator = new ResourceGenerator(template);
                player.resourceGenerators.Add(generator.guid, generator);
            }
        }
    }
}