﻿using Economy;
using ECS;
using ECS.Jobs.Datas;
using ECS.Tools;
using Global;
using Global.Other;
using Newtonsoft.Json;
using Terrain;
using Units.Factories;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace LockedGlobal
{
    public class MainGameObject : MonoBehaviour
    {
        public GameTimer timer = new GameTimer(60 * 2);
        private int height = 100;
        private int width = 100;

        void Start()
        {
            // Application.targetFrameRate = 60;
            Managers.Instantiate();
            UnitFactory.LoadUnitTemplates();
            _terrainGenerator = new TerrainGenerator();
            _terrainGenerator.GenerateRandomTerrain(height, width);
            EcsManager.mapNativeData = new MapNativeData(_terrainGenerator.heights, height, width, _terrainGenerator.scale);
            Test();
            // UnsafeTest();
        }

        private void Test()
        {
            var player1 = new Player("1");
            var player2 = new Player("2");
            var unitCount = 10000;//this instantiates 2n units
            Managers.playerManager.AddPlayer("me", "0");
            Managers.playerManager.AddPlayer("enemy", "0");
            Managers.playerManager.AddPassiveResource();
            // for (int i = 0; i < unitCount; i++)
            // {
            //     UnitFactory.Instantiate(UnitFactory.templates["DefaultNavy"], Randoms.RandomVector3Flat(50) + new Vector3(20, 0, 20), player1);
            //     UnitFactory.Instantiate(UnitFactory.templates["DefaultNavy"], new Vector3(20, 0, 500), player2);
            // }
            for (int i = 0; i < unitCount; i++)
            {
                UnitFactory.Instantiate(UnitFactory.templates["DefaultAir"], Randoms.RandomVector3Flat(500) + new Vector3(1000, 50, 500), player1);
                UnitFactory.Instantiate(UnitFactory.templates["DefaultAir"], Randoms.RandomVector3Flat(500) + new Vector3(0, 50, 500), player2);
            }
        }

        private unsafe void UnsafeTest()
        {
            Cuda.Cuda.GenerateNavMesh((float*) _terrainGenerator.heights.GetUnsafePtr(), height, width);
        }

        private void Update()
        {
            // timer.DecrementTimer();
            Managers.Tick();
            EcsManager.Tick();
            // Debug.Log("World mouse pos " + GamePhysics.GetMousePositionInWorld());
            var mousePos = GamePhysics.GetMousePositionInWorld();
            Managers.unitManager.SetAllUnitsTarget(GamePhysics.GetMousePositionInWorld());
        }

        private TerrainGenerator _terrainGenerator;

        private void GenerateRandomUnit()
        {
            var unit = RandomFactories.GetRandomUnit();
            var text = JsonConvert.SerializeObject(unit, Globals.jsonSettings);
            Debug.Log(text);
        }
    }
}