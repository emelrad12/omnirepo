﻿using System;
using ECS.Jobs.Datas;
using Global.Templates;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace ECS.Jobs
{
    [BurstCompile]
    public struct UnitMovementJob : IJobParallelFor
    {
        public UnitNativeData unitNativeData;
        public MapNativeData mapNativeData;

        public void Execute(int i)
        {
            if (!unitNativeData.IsAlive(i))
            {
                return;
            }

            var additionalUnitData = unitNativeData.additionalUnitDatas[i];

            if (CanMoveForward(unitNativeData.velocities[i], unitNativeData.positions[i], additionalUnitData.type))
            {
                unitNativeData.positions[i] += unitNativeData.velocities[i];
                unitNativeData.isStuck[i] = false;
            }
            else
            {
                unitNativeData.velocities[i] = 0;
                unitNativeData.isStuck[i] = true;
            }

            float3 currentPosition = unitNativeData.positions[i];
            var currentRotation = unitNativeData.rotations[i];
            var currentSpeed = Vector3.Magnitude(unitNativeData.velocities[i]);
            var toTargetDistance = Vector3.Magnitude(unitNativeData.targetLocation[i] - currentPosition);
            var stoppingDistance = currentSpeed * currentSpeed / additionalUnitData.acceleration + 1;
            var acceleration = additionalUnitData.acceleration;
            var desiredSpeedMultiplier = 1.0;
            float targetAltitude = 50;
            float minAltitude = 10;
            float3 adjustedTargetPosition = unitNativeData.targetLocation[i];
            float minSpeed = 0;
            if (additionalUnitData.type == MovementTemplate.MovementType.Naval)
            {
                var asEuler = ((Quaternion) currentRotation).eulerAngles;
                asEuler.x = 0;
                asEuler.z = 0;
                unitNativeData.rotations[i] = Quaternion.Euler(asEuler);
            }


            if (additionalUnitData.type == MovementTemplate.MovementType.Air)
            {
                if (currentPosition.y < minAltitude)
                {
                    adjustedTargetPosition.y = targetAltitude;
                }

                minSpeed = 0.25f * additionalUnitData.maxSpeed;
            }

            float3 velocity = math.mul(unitNativeData.rotations[i], Vector3.forward);
            unitNativeData.velocities[i] = velocity * Vector3.Magnitude(unitNativeData.velocities[i]);
            float3 relativePos = adjustedTargetPosition - currentPosition;
            if (Vector3.Magnitude(relativePos) > 1f)
            {
                Quaternion toTargetRotation = Quaternion.LookRotation(relativePos, Vector3.up);
                if (unitNativeData.shouldBroadside[i])
                {
                    toTargetRotation *= quaternion.RotateY(90 * Mathf.Deg2Rad);
                }

                if (Quaternion.Angle(toTargetRotation, currentRotation) > 25)
                {
                    desiredSpeedMultiplier = 0.25;
                }
                unitNativeData.rotations[i] = Quaternion.RotateTowards(currentRotation, toTargetRotation, additionalUnitData.turnRate);
            }


            if (unitNativeData.stayStill[i])
            {
                Accelerate(i, -acceleration, minSpeed);
            }
            else
            {
                if (toTargetDistance < stoppingDistance)
                {
                    Accelerate(i, -acceleration, minSpeed);
                }
                else
                {
                    if (currentSpeed < additionalUnitData.maxSpeed * desiredSpeedMultiplier)
                    {
                        Accelerate(i, acceleration, minSpeed);
                    }
                }
            }
        }

        public bool CanMoveForward(Vector3 direction, Vector3 position, MovementTemplate.MovementType movementType)
        {
            var nextPoint = position + direction;
            switch (movementType)
            {
                case MovementTemplate.MovementType.Land:
                    return mapNativeData.GetHeightAt(nextPoint.x, nextPoint.z) > 0;
                case MovementTemplate.MovementType.Air:
                    return true;
                case MovementTemplate.MovementType.Gunship:
                    return true;
                case MovementTemplate.MovementType.Naval:
                    return mapNativeData.GetHeightAt(nextPoint.x, nextPoint.z) < -2;
                case MovementTemplate.MovementType.Amphibious:
                    return true;
                case MovementTemplate.MovementType.Hover:
                    return true;
                case MovementTemplate.MovementType.None:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException(nameof(movementType), movementType, "Unknown Movement Type: " + movementType);
            }
        }

        public void Accelerate(int i, float acceleration, float minSpeed = 0)
        {
            var currentSpeed = Vector3.Magnitude(unitNativeData.velocities[i]);
            if (acceleration < 0 && currentSpeed + acceleration < minSpeed)
            {
                return;
            }

            if (acceleration < 0 && Vector3.Magnitude(unitNativeData.velocities[i]) < acceleration)
            {
                unitNativeData.velocities[i] = 0;
            }
            else
            {
                unitNativeData.velocities[i] += math.mul(unitNativeData.rotations[i], Vector3.forward) * acceleration;
            }
        }
    }
}