﻿using System;
using ECS.Jobs.Datas;
using Global.Other;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

namespace ECS.Jobs
{
    [BurstCompile]
    public struct WeaponJob : IJobParallelFor
    {
        public WeaponJob(WeaponNativeData weaponNativeData, UnitNativeData unitNativeData)
        {
            this.weaponNativeData = weaponNativeData;
            this.unitNativeData = unitNativeData;
        }

        public WeaponNativeData weaponNativeData;
        public readonly UnitNativeData unitNativeData;

        public void Execute(int i)
        {
            int unitSlot = weaponNativeData.unitSlot[i];
            if (!unitNativeData.IsAlive(unitSlot) || !weaponNativeData.isAlive[i])
            {
                weaponNativeData.isAlive[i] = false;
                return;
            }

            int targetSlot = weaponNativeData.targetedUnitSlot[i];
            bool isTargetAlive = unitNativeData.IsAlive(targetSlot);

            var additionalData = weaponNativeData.additionalTurretDatas[i];
            var additionalWeaponData = weaponNativeData.additionalWeaponDatas[i];
            var rotation = unitNativeData.rotations[unitSlot];
            weaponNativeData.positions[i] = unitNativeData.positions[unitSlot] + math.mul(rotation, weaponNativeData.offsets[i]);
            float3 position = weaponNativeData.positions[i];
            float3 targetLocation = unitNativeData.positions[targetSlot];
            float3 relativePos = targetLocation - position;
            quaternion weaponRotation = math.mul(additionalWeaponData.defaultRotation, rotation);
            if (weaponNativeData.hasTurret[i])
            {
                var relativeFlatPos = relativePos;
                relativeFlatPos.y = 0;
                float shellSpeed = !additionalWeaponData.drop ? Int32.MaxValue : additionalWeaponData.speed;
                var distance = Vector3.Magnitude(relativeFlatPos) * 0.995f;
                float2 angleToShell = HeavyMath.AngleToFireShell(shellSpeed, distance, relativePos.y);
                if (!float.IsNaN(angleToShell.x) && isTargetAlive && distance < additionalWeaponData.range)
                {
                    float3 targetLocationAdjustedForShell = HeavyMath.ExtendUpwardsByAngle(angleToShell.y, position, targetLocation);
                    float3 adjustedRelativeLocation = targetLocationAdjustedForShell - position;
                    var anglesToTarget = new float2(
                        HeavyMath.AngleToTargetOnRotatedPlane(weaponRotation, adjustedRelativeLocation, false),
                        HeavyMath.AngleToTargetOnRotatedPlane(weaponNativeData.turretRotations[i], adjustedRelativeLocation, true));
                    weaponNativeData.turretAndBarrelRotations[i] = LightMath.LerpVector2(
                        weaponNativeData.turretAndBarrelRotations[i],
                        anglesToTarget,
                        additionalData.turnRate);
                    weaponNativeData.turretAndBarrelRotations[i] = new float2(
                        ClampTurretRotation(false, additionalWeaponData, weaponNativeData.turretAndBarrelRotations[i].x),
                        ClampTurretRotation(false, additionalWeaponData, weaponNativeData.turretAndBarrelRotations[i].y));
                    Quaternion toTargetRotation = Quaternion.LookRotation(adjustedRelativeLocation, Vector3.up);
                    weaponNativeData.currentAngleToTarget[i] = Mathf.Abs(Quaternion.Angle(weaponNativeData.barrelRotations[i], toTargetRotation));
                }
                else
                {
                    weaponNativeData.turretAndBarrelRotations[i] = LightMath.LerpVector2(
                        weaponNativeData.turretAndBarrelRotations[i],
                        float2.zero,
                        additionalData.turnRate);
                    weaponNativeData.currentAngleToTarget[i] = 99999;
                }

                weaponNativeData.turretRotations[i] = weaponRotation * Quaternion.Euler(new Vector3(0, weaponNativeData.turretAndBarrelRotations[i].x, 0));
                weaponNativeData.barrelRotations[i] = weaponNativeData.turretRotations[i] * Quaternion.Euler(new Vector3(weaponNativeData.turretAndBarrelRotations[i].y, 0, 0));
                weaponNativeData.barrelPositions[i] = position + math.mul(weaponNativeData.barrelRotations[i], additionalData.barrelOffset);
                weaponNativeData.barrelOutputPosition[i] = weaponNativeData.barrelPositions[i] + math.mul(weaponNativeData.barrelRotations[i], (new float3(0, 0, 1)) * additionalData.barrelLength);
            }
            else
            {
                var anglesToTarget = new float2(
                    HeavyMath.AngleToTargetOnRotatedPlane(weaponRotation, relativePos, false),
                    HeavyMath.AngleToTargetOnRotatedPlane(weaponRotation, relativePos, true));
                weaponNativeData.turretAndBarrelRotations[i] = LightMath.LerpVector2(
                    weaponNativeData.turretAndBarrelRotations[i],
                    anglesToTarget,
                    1000);
                weaponNativeData.turretAndBarrelRotations[i] = new float2(
                    ClampTurretRotation(false, additionalWeaponData, weaponNativeData.turretAndBarrelRotations[i].x),
                    ClampTurretRotation(false, additionalWeaponData, weaponNativeData.turretAndBarrelRotations[i].y));
                weaponNativeData.barrelRotations[i] = math.mul(rotation, additionalWeaponData.defaultRotation) *
                                                      Quaternion.Euler(new Vector3(weaponNativeData.turretAndBarrelRotations[i].y, 0, 0)) *
                                                      Quaternion.Euler(new Vector3(0, weaponNativeData.turretAndBarrelRotations[i].x, 0));
                weaponNativeData.barrelOutputPosition[i] = math.mul(weaponRotation, additionalData.barrelOffset) + position;
            }
        }

        public float ClampTurretRotation(bool isBarrel, AdditionalWeaponData data, float targetAngle)
        {
            var limits = isBarrel ? data.yLimits : data.xLimits;
            if (targetAngle < limits.x)
            {
                return limits.x;
            }

            if (targetAngle > limits.y)
            {
                return limits.y;
            }

            return targetAngle;
        }
    }
}