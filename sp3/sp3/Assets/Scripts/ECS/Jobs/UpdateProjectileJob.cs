﻿using ECS.Jobs.Datas;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace ECS.Jobs
{
    [BurstCompile]
    public struct UpdateProjectileJob : IJobParallelFor
    {
        public UpdateProjectileJob(ProjectileNativeData projectileNativeData, UnitNativeData unitNativeData, WeaponNativeData weaponNativeData)
        {
            this.projectileNativeData = projectileNativeData;
            this.unitNativeData = unitNativeData;
            this.weaponNativeData = weaponNativeData;
        }

        public ProjectileNativeData projectileNativeData;
        public readonly UnitNativeData unitNativeData;
        public readonly WeaponNativeData weaponNativeData;

        public void Execute(int i)
        {
            if (!projectileNativeData.isAlive[i] || projectileNativeData.toCollect[i])
            {
                return;
            }

            var additionalData = projectileNativeData.additionalProjectileDatas[i];
            projectileNativeData.rotations[i] = Quaternion.LookRotation(projectileNativeData.velocities[i]);

            if (additionalData.homing)
            {
                if (!unitNativeData.IsAlive(projectileNativeData.targetedUnitSlot[i]))
                {
                    projectileNativeData.targetedUnitSlot[i] = weaponNativeData.targetedUnitSlot[additionalData.parentSlot];
                }

                var targetLocation = unitNativeData.positions[projectileNativeData.targetedUnitSlot[i]];
                float3 relativePos = targetLocation - projectileNativeData.positions[i];
                quaternion toTargetRotation = quaternion.LookRotation(relativePos, Vector3.up);
                var newRotation = Quaternion.RotateTowards(projectileNativeData.rotations[i], toTargetRotation, additionalData.turnRate);
                projectileNativeData.rotations[i] = newRotation;
                float3 velocity = math.mul(newRotation, Vector3.forward);
                projectileNativeData.velocities[i] = velocity * additionalData.speed;
            }
        }
    }
}