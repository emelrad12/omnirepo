﻿using System.Diagnostics.Contracts;
using Global.Templates;
using Unity.Collections;
using Unity.Mathematics;

namespace ECS.Jobs.Datas
{
    public struct UnitNativeData
    {
        public UnitNativeData(int size)
        {
            capacity = size;
            positions = new NativeArray<float3>(size, Allocator.Persistent);
            velocities = new NativeArray<float3>(size, Allocator.Persistent);
            targetLocation = new NativeArray<float3>(size, Allocator.Persistent);
            rotations = new NativeArray<quaternion>(size, Allocator.Persistent);
            additionalUnitDatas = new NativeArray<AdditionalUnitData>(size, Allocator.Persistent);
            stayStill = new NativeArray<bool>(size, Allocator.Persistent);
            health = new NativeArray<float>(size, Allocator.Persistent);
            toCollect = new NativeArray<bool>(size, Allocator.Persistent);
            isStuck = new NativeArray<bool>(size, Allocator.Persistent);
            shouldBroadside = new NativeArray<bool>(size, Allocator.Persistent);
            actualUsed = 0;
        }
        [Pure]
        public bool IsAlive(int slot)
        {
            return health[slot] > 0;
        }

        public int capacity;
        public int actualUsed;
        public NativeArray<float3> positions;
        public NativeArray<bool> toCollect;
        public NativeArray<bool> shouldBroadside;
        public NativeArray<float> health;
        public NativeArray<bool> stayStill;
        public NativeArray<bool> isStuck;
        public NativeArray<float3> velocities;
        public NativeArray<quaternion> rotations;
        public NativeArray<float3> targetLocation;
        public NativeArray<AdditionalUnitData> additionalUnitDatas;
    }

    public struct AdditionalUnitData
    {
        public float maxSpeed;
        public float turnRate;
        public float acceleration;
        public MovementTemplate.MovementType type;
        public float2 heightLimits;
        public int allegiance;
        public bool broadside;
    }
}