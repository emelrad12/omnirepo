﻿using Unity.Collections;
using Unity.Mathematics;

namespace ECS.Jobs.Datas
{
    public struct WeaponNativeData
    {
        public WeaponNativeData(int size)
        {
            hasTurret = new NativeArray<bool>(size, Allocator.Persistent);
            unitSlot = new NativeArray<int>(size, Allocator.Persistent);
            offsets = new NativeArray<float3>(size, Allocator.Persistent);
            positions = new NativeArray<float3>(size, Allocator.Persistent);
            turretRotations = new NativeArray<quaternion>(size, Allocator.Persistent);
            additionalTurretDatas = new NativeArray<AdditionalTurretData>(size, Allocator.Persistent);
            targetedUnitSlot = new NativeArray<int>(size, Allocator.Persistent);
            barrelPositions = new NativeArray<float3>(size, Allocator.Persistent);
            barrelOutputPosition = new NativeArray<float3>(size, Allocator.Persistent);
            barrelRotations = new NativeArray<quaternion>(size, Allocator.Persistent);
            turretAndBarrelRotations = new NativeArray<float2>(size, Allocator.Persistent);
            additionalWeaponDatas = new NativeArray<AdditionalWeaponData>(size, Allocator.Persistent);
            isAlive = new NativeArray<bool>(size, Allocator.Persistent);
            currentAngleToTarget = new NativeArray<float>(size, Allocator.Persistent);
            toCollect = new NativeArray<bool>(size, Allocator.Persistent);
            isInRange = new NativeArray<bool>(size, Allocator.Persistent);
            actualUsed = 0;
            capacity = size;
        }

        public NativeArray<bool> isAlive;
        public NativeArray<bool> isInRange;
        public NativeArray<int> unitSlot;
        public NativeArray<float3> positions;
        public NativeArray<float3> barrelPositions;
        public NativeArray<quaternion> barrelRotations;
        public NativeArray<float3> barrelOutputPosition;
        public NativeArray<float2> turretAndBarrelRotations;
        public NativeArray<bool> hasTurret;
        public NativeArray<float3> offsets;
        public NativeArray<quaternion> turretRotations;
        public NativeArray<int> targetedUnitSlot;
        public NativeArray<AdditionalTurretData> additionalTurretDatas;
        public NativeArray<AdditionalWeaponData> additionalWeaponDatas;
        public NativeArray<float> currentAngleToTarget;
        public NativeArray<bool> toCollect;
        public int actualUsed;
        public int capacity;
    }

    public struct AdditionalTurretData
    {
        public float turnRate;
        public float3 barrelOffset;
        public float barrelLength;
    }
    
    public struct AdditionalWeaponData
    {
        public quaternion defaultRotation;
        public bool homing;
        public bool drop;
        public float speed;
        public float range;
        public float2 yLimits;
        public float2 xLimits;
    }
}