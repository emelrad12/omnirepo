﻿using System;
using Unity.Collections;

namespace ECS.Jobs.Datas
{
    public struct MapNativeData
    {
        public MapNativeData(NativeArray<float> heights, int width, int length, float scale)
        {
            this.width = width + 1;
            this.length = length + 1;
            this.heights = heights;
            this.scale = scale;
        }

        public float GetHeightAt(float x, float z)
        {
            x = (int) (x / scale);
            z = (int) (z / scale);
            if (z < 0 || x < 0 || x > width || z > width)
            {
                return 99999;
            }
            return heights[(int) (x + z * width)];
        }

        public float scale;
        public int width;
        public int length;
        public readonly NativeArray<float> heights;
    }
}