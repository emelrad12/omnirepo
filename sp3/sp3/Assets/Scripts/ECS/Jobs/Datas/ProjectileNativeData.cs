﻿using Unity.Collections;
using Unity.Mathematics;

namespace ECS.Jobs.Datas
{
    public struct ProjectileNativeData
    {
        public ProjectileNativeData(int size)
        {
            capacity = size;
            actualUsed = 0;
            positions = new NativeArray<float3>(size, Allocator.Persistent);
            velocities = new NativeArray<float3>(size, Allocator.Persistent);
            rotations = new NativeArray<quaternion>(size, Allocator.Persistent);
            targetedUnitSlot = new NativeArray<int>(size, Allocator.Persistent);
            isAlive = new NativeArray<bool>(size, Allocator.Persistent);
            toCollect = new NativeArray<bool>(size, Allocator.Persistent);
            additionalProjectileDatas = new NativeArray<AdditionalProjectileData>(size, Allocator.Persistent);
        }

        public int capacity;
        public int actualUsed;
        public NativeArray<float3> positions;
        public NativeArray<float3> velocities;
        public NativeArray<quaternion> rotations;
        public NativeArray<int> targetedUnitSlot;
        public NativeArray<bool> isAlive;
        public NativeArray<bool> toCollect;
        public NativeArray<AdditionalProjectileData> additionalProjectileDatas;
    }
    public struct AdditionalProjectileData
    {
        public float speed;
        public bool homing;
        public bool drop;
        public float turnRate;
        public float size;
        public float liveTill;
        public float damage;
        public int allegiance;
        public int parentSlot;
    }
}