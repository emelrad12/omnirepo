﻿using Unity.Jobs;
using UnityEngine;

namespace ECS.Jobs
{
    public class JobManager
    {
        public UnitMovementJob unitMovementJob;
        public UpdateProjectileJob updateProjectileJob;
        public WeaponJob weaponJob;

        public void Update()
        {
            JobHandle handle;
            unitMovementJob = new UnitMovementJob();
            unitMovementJob.unitNativeData = EcsManager.ecsUnitManager.unitNativeData;
            unitMovementJob.mapNativeData = EcsManager.mapNativeData;
            handle = unitMovementJob.Schedule(EcsManager.ecsUnitManager.unitNativeData.capacity, 12);
            handle.Complete();
            updateProjectileJob = new UpdateProjectileJob(
                EcsManager.ecsWeaponManager.projectileManager.projectileNativeData, 
                EcsManager.ecsUnitManager.unitNativeData,
                EcsManager.ecsWeaponManager.weaponNativeData);
            handle = updateProjectileJob.Schedule(EcsManager.ecsWeaponManager.projectileManager.projectileNativeData.capacity, 12);
            handle.Complete();
            weaponJob = new WeaponJob(EcsManager.ecsWeaponManager.weaponNativeData, EcsManager.ecsUnitManager.unitNativeData);
            handle = weaponJob.Schedule(EcsManager.ecsWeaponManager.weaponNativeData.capacity, 12);
            handle.Complete();
        }

        public int GetAdjustedItemCount(int actualUsed, int total, int bonus)
        {
            return Mathf.Min(total, actualUsed + bonus);
        }
    }
}