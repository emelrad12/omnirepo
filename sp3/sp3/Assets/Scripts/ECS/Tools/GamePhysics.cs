﻿using Global;
using UnityEngine;

namespace ECS.Tools
{
    public static class GamePhysics
    {
        public static Vector3 GetMousePositionInWorld()
        {
            Ray ray = Camera.main.ViewportPointToRay(Camera.main.ScreenToViewportPoint(Input.mousePosition));
            var from = Globals.GetCameraPosition();
            var to = from + ray.direction * 10000;
            return Raycast.Cast(from, to);
        }
    }
}