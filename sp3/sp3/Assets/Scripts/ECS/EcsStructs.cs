﻿using System;
using ECS.Jobs;
using ECS.Managers;
using Unity.Entities;

namespace ECS
{
    public struct EcsUnitItem : IComponentData
    {
        public enum Type
        {
            Projectile,
            Unit,
            Terrain,
            Weapon,
        }

        public EcsUnitItem(EcsProjectileManager.Projectile projectile)
        {
            slot = projectile.ecsProjectileData.slot;
            type = Type.Projectile;
        }

        public EcsUnitItem(EcsWeaponManager.EcsWeapon weapon)
        {
            slot = weapon.slot;
            type = Type.Weapon;
        }

        public EcsUnitItem(EcsUnit ecsUnit)
        {
            slot = ecsUnit.slot;
            type = Type.Unit;
        }
        
        public EcsUnitItem(Type type)
        {
            slot = -1;
            this.type = type;
        }

        public Type type;
        public int slot;
    }

    public struct EcsProjectileComponent : IComponentData
    {
        public bool homing;
    }

    public struct EcsUnitComponent : IComponentData
    {
        private bool placeholder;
    }

    public struct EcsTurretComponent : IComponentData
    {
        private bool placeholder;
    }

    public struct EcsBarrelComponent : IComponentData
    {
        private bool placeholder;
    }
}