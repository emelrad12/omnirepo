﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using ECS.Jobs.Datas;
using Global;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Collider = Unity.Physics.Collider;
using Material = UnityEngine.Material;
using SphereCollider = Unity.Physics.SphereCollider;

namespace ECS.Managers
{
    public class EcsProjectileManager
    {
        public ProjectileNativeData projectileNativeData;
        public List<Entity> entities;
        public int size;

        public void Init(int size)
        {
            projectileNativeData = new ProjectileNativeData(size);
            entities = new List<Entity>(size);
            _unitSlots = new FreeArraySpaceTracker(size);
            this.size = size;
            for (int i = 0; i < size; i++)
            {
                var entityManager = EcsManager.entityManager;
                EntityArchetype entityArchetype = entityManager.CreateArchetype(
                    typeof(Translation),
                    typeof(RenderMesh),
                    typeof(LocalToWorld),
                    typeof(Scale),
                    typeof(Rotation),
                    typeof(RenderBounds),
                    typeof(EcsUnitItem),
                    typeof(EcsProjectileComponent),
                    typeof(PhysicsVelocity),
                    typeof(PhysicsCollider),
                    typeof(PhysicsMass),
                    typeof(PhysicsDamping),
                    typeof(PhysicsGravityFactor),
                    typeof(Disabled)
                );
                entities.Add(entityManager.CreateEntity(entityArchetype));
            }
        }


        public void Update()
        {
            foreach (var projectile in _projectilesQueue)
            {
                SpawnBullet(projectile);
            }

            projectileNativeData.actualUsed = _unitSlots.UsedSpace;
            _projectilesQueue = new ConcurrentBag<Projectile>();
            FreeSpaces();
        }

        public void AddProjectile(Projectile projectile)
        {
            _projectilesQueue.Add(projectile);
        }

        public Projectile GetNewProjectile()
        {
            return new Projectile
            {
                ecsProjectileData = new EcsProjectileData
                {
                    slot = _unitSlots.GetSpace()
                }
            };
        }

        public struct Projectile
        {
            public Mesh mesh;
            public Material material;
            public EcsProjectileData ecsProjectileData;
        }

        public struct EcsProjectileData
        {
            public Quaternion rotation;
            public float damage;
            public Vector3 position;
            public float size;
            public int targetSlot;
            public bool homing;
            public bool drop;
            public float speed;
            public int slot;
            public float turnRate;
            public float lifespan;
            public int allegiance;
            public int parentSlot;
        }

        private void FreeSpaces()
        {
            for (int i = 0; i < projectileNativeData.capacity; i++)
            {
                var item = projectileNativeData.additionalProjectileDatas[i];
                if (projectileNativeData.toCollect[i])
                {
                    FreeSlot(i);
                    continue;
                }

                if (projectileNativeData.isAlive[i] && Time.time > item.liveTill)
                {
                    FreeSlot(i);
                }
            }
        }

        private void FreeSlot(int slot)
        {
            if (!projectileNativeData.isAlive[slot])
            {
                throw new Exception("Cannot collect dead projectile at: " + slot);
            }

            projectileNativeData.isAlive[slot] = false;
            projectileNativeData.toCollect[slot] = false;
            _unitSlots.FreeSpace(slot);
        }

        private unsafe void SpawnBullet(Projectile projectile)
        {
            var ecsData = projectile.ecsProjectileData;
            var slot = projectile.ecsProjectileData.slot;
            if (projectileNativeData.isAlive[slot])
            {
                throw new Exception("Cannot override projectile: " + slot);
            }

            projectileNativeData.positions[slot] = ecsData.position;
            projectileNativeData.rotations[slot] = ecsData.rotation;
            projectileNativeData.targetedUnitSlot[slot] = ecsData.targetSlot;
            projectileNativeData.isAlive[slot] = true;
            projectileNativeData.toCollect[slot] = false;
            projectileNativeData.velocities[slot] = math.mul(ecsData.rotation, Vector3.forward) * ecsData.speed;
            projectileNativeData.additionalProjectileDatas[slot] = new AdditionalProjectileData
            {
                damage = ecsData.damage,
                drop = ecsData.drop,
                speed = ecsData.speed,
                homing = ecsData.homing,
                turnRate = ecsData.turnRate,
                size = ecsData.size,
                liveTill = Time.time + projectile.ecsProjectileData.lifespan,
                allegiance = projectile.ecsProjectileData.allegiance,
                parentSlot = projectile.ecsProjectileData.parentSlot
            };
            var entityManager = EcsManager.entityManager;
            var entity = entities[slot];
            var renderMesh = new RenderMesh
            {
                mesh = projectile.mesh,
                material = projectile.material
            };
            uint mask = 1;
            var material = Unity.Physics.Material.Default;
            material.Flags = Unity.Physics.Material.MaterialFlags.EnableCollisionEvents |
                             Unity.Physics.Material.MaterialFlags.EnableMassFactors |
                             Unity.Physics.Material.MaterialFlags.EnableSurfaceVelocity;
            var collider = SphereCollider.Create(new SphereGeometry
            {
                Center = float3.zero,
                Radius = 0.5f
            }, new CollisionFilter
            {
                BelongsTo = mask,
                CollidesWith = ~mask
            }, material);

            Collider* colliderPtr = (Collider*) collider.GetUnsafePtr();
            entityManager.SetSharedComponentData(entity, renderMesh);
            entityManager.SetComponentData(entity, new PhysicsVelocity {Linear = projectileNativeData.velocities[slot], Angular = float3.zero});
            entityManager.SetComponentData(entity, PhysicsMass.CreateDynamic(colliderPtr->MassProperties, 1));
            entityManager.SetComponentData(entity, new PhysicsGravityFactor {Value = ecsData.drop ? 1 : 0});
            entityManager.SetComponentData(entity, new PhysicsCollider {Value = collider});
            entityManager.SetComponentData(entity, new PhysicsDamping {Linear = 0.00f, Angular = 0.00f});
            entityManager.SetComponentData(entity, new Rotation {Value = projectile.ecsProjectileData.rotation});
            entityManager.SetComponentData(entity, new Translation {Value = projectileNativeData.positions[slot]});
            entityManager.SetComponentData(entity, new Scale {Value = ecsData.size});
            entityManager.SetComponentData(entity, new EcsUnitItem(projectile));
            entityManager.SetComponentData(entity, new EcsProjectileComponent
            {
                homing = projectile.ecsProjectileData.homing
            });
        }

        private FreeArraySpaceTracker _unitSlots;
        private ConcurrentBag<Projectile> _projectilesQueue = new ConcurrentBag<Projectile>();
    }
}