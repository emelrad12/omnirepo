﻿using ECS.Jobs.Datas;
using Global;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using Collider = Unity.Physics.Collider;
using Material = UnityEngine.Material;

namespace ECS.Managers
{
    public class EcsUnitManager
    {
        public UnitNativeData unitNativeData;
        public void Init(int size)
        {
            unitNativeData = new UnitNativeData(size);
            _unitSlots = new FreeArraySpaceTracker(size);
        }

        public void Update()
        {
            CollectDeadUnits();
            unitNativeData.actualUsed = _unitSlots.UsedSpace;
        }

        public EcsUnit GetEcsUnit()
        {
            return new EcsUnit
            {
                slot = _unitSlots.GetSpace()
            };
        }

        public void CollectDeadUnits()
        {
            for (int i = 0; i < unitNativeData.actualUsed; i++)
            {
                if (unitNativeData.health[i] < 0)
                {
                    _unitSlots.FreeSpace(i);
                }
            }
        }
        

        public void SpawnUnit(EcsUnit ecsUnit)
        {
            unitNativeData.positions[ecsUnit.slot] = ecsUnit.position;
            unitNativeData.rotations[ecsUnit.slot] = ecsUnit.rotation;
            unitNativeData.velocities[ecsUnit.slot] = float3.zero;
            unitNativeData.health[ecsUnit.slot] = ecsUnit.health;
            unitNativeData.additionalUnitDatas[ecsUnit.slot] = ecsUnit.additionalUnitData;
            var entityManager = EcsManager.entityManager;
            var renderMesh = new RenderMesh
            {
                mesh = ecsUnit.mesh,
                material = ecsUnit.material
            };
            EntityArchetype entityArchetype = entityManager.CreateArchetype(
                typeof(Translation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Scale),
                typeof(Rotation),
                typeof(RenderBounds),
                typeof(EcsUnitItem),
                typeof(EcsUnitComponent),
                typeof(PhysicsCollider)
            );
            var ecsUnitItem = new EcsUnitItem(ecsUnit);
            var entity = entityManager.CreateEntity(entityArchetype);
            entityManager.SetSharedComponentData(entity, renderMesh);
            entityManager.SetComponentData(entity, new PhysicsCollider {Value = ecsUnit.collider});
            entityManager.AddComponentData(entity, new Rotation {Value = ecsUnit.rotation});
            entityManager.SetComponentData(entity, new Translation {Value = new float3(ecsUnit.position)});
            entityManager.SetComponentData(entity, new Scale {Value = 1});
            entityManager.SetComponentData(entity, ecsUnitItem);
        }

        private FreeArraySpaceTracker _unitSlots;
    }

    public class EcsUnit
    {
        public int slot;
        public Mesh mesh;
        public BlobAssetReference<Collider> collider;
        public Material material;
        public Vector3 position;
        public float health;
        public Quaternion rotation;
        public AdditionalUnitData additionalUnitData;
    }
}