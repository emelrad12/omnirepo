﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using Global;
using UnityEngine;

namespace ECS.Managers
{
    public class BeamManager
    {
        private static List<GameObject> _laserGameObjects;
        private static List<LineRenderer> _laserRenderers;
        private static ConcurrentBag<Beam> _lasers;
        private static int _nextFreeLaserIndex;
        public int laserCount = 20;

        public BeamManager()
        {
            Start();
        }

        public void Start()
        {
            _laserGameObjects = new List<GameObject>();
            _laserRenderers = new List<LineRenderer>();
            _lasers = new ConcurrentBag<Beam>();
            for (int i = 0; i < laserCount; i++)
            {
                _laserGameObjects.Add(new GameObject());
            }

            foreach (var lineObject in _laserGameObjects)
            {
                var laserRenderer = lineObject.AddComponent<LineRenderer>();
                laserRenderer.positionCount = 2;
                _laserRenderers.Add(laserRenderer);
            }
        }

        public void Render()
        {
            Reset();
            if (_lasers.Count > laserCount)
            {
                laserCount = (int) (laserCount * 1.25f);
                Start();
                return;
            }

            foreach (var laser in _lasers)
            {
                var laserRenderer = _laserRenderers[_nextFreeLaserIndex++];
                laserRenderer.positionCount = 2;
                laserRenderer.SetPosition(0, laser.start);
                laserRenderer.SetPosition(1, laser.end);
                laserRenderer.material = GetLaserMaterial(laser.color);
                laserRenderer.startWidth = laser.Width;
                laserRenderer.endWidth = laser.Width / 2;
            }
            _lasers = new ConcurrentBag<Beam>();
        }

        public class Beam
        {
            public Vector3 start;
            public Vector3 end;
            public Color color = Color.red;
            public float power;

            public Vector3 Middle => Vector3.Lerp(start, end, 0.5f);
            public float Width => Globals.ClampsLaserWidth(Mathf.Sqrt(power), Middle);
        }

        public void AddBeam(Beam beam)
        {
            _lasers.Add(beam);
        }

        private void Reset()
        {
            _nextFreeLaserIndex = 0;
            foreach (var laserRenderer in _laserRenderers)
            {
                laserRenderer.positionCount = 2;
            }
        }
        
        private Material GetLaserMaterial(Color? color = null)
        {
            if (color.HasValue)
            {
                if (_materials.ContainsKey(color.Value))
                {
                    return _materials[color.Value];
                }

                var newMat = new Material(_laserMaterial);
                newMat.SetColor("LaserColor", color.Value);
                _materials.Add(color.Value, newMat);
                return newMat;
            }

            return _laserMaterial;
        }
        private Material _laserMaterial = Resources.Load<Material>("Materials/Laser/Default"); //todo
        private Dictionary<Color, Material> _materials = new Dictionary<Color, Material>();
    }
}