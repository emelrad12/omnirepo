﻿using ECS.Jobs.Datas;
using Global;
using Unity.Mathematics;
using UnityEngine;

namespace ECS.Managers
{
    public class EcsWeaponManager
    {
        public EcsWeaponManager()
        {
            projectileManager = new EcsProjectileManager();
        }

        public WeaponNativeData weaponNativeData;
        public EcsProjectileManager projectileManager;

       

        public void Init(int size)
        {
            weaponNativeData = new WeaponNativeData(size);
            _unitSlots = new FreeArraySpaceTracker(size);
            projectileManager.Init(size);
        }

        public void Update()
        {
            CollectDeadUnits();
            weaponNativeData.actualUsed = _unitSlots.UsedSpace;
            projectileManager.Update();
        }

        public EcsWeapon GetNewWeapon()
        {
            return new EcsWeapon
            {
                slot = _unitSlots.GetSpace(),
            };
        }

        public void AddWeapon(EcsWeapon weapon)
        {
            EcsManager.ecsTurretManager.AddTurret(weapon);
            weaponNativeData.isAlive[weapon.slot] = true;
            weaponNativeData.hasTurret[weapon.slot] = weapon.hasTurret;
            weaponNativeData.unitSlot[weapon.slot] = weapon.unitSlot;
            weaponNativeData.offsets[weapon.slot] = weapon.offset;
            if (weapon.hasTurret)
            {
                weaponNativeData.additionalTurretDatas[weapon.slot] = new AdditionalTurretData
                {
                    barrelOffset = weapon.turret.ecsTurretData.barrelOffset,
                    turnRate = weapon.turret.ecsTurretData.rotationSpeed,
                    barrelLength = weapon.turret.ecsTurretData.barrelLength
                }; 
            }
            weaponNativeData.additionalWeaponDatas[weapon.slot] = new AdditionalWeaponData
            {
                drop = weapon.drop,
                homing = weapon.homing,
                speed = weapon.shellSpeed,
                defaultRotation = weapon.defaultRotation,
                xLimits = weapon.xLimits,
                yLimits = weapon.yLimits,
                range = weapon.range
            };
        }
        
        private void CollectDeadUnits()
        {
            for (int i = 0; i < weaponNativeData.actualUsed; i++)
            {
                if (!weaponNativeData.isAlive[i] )
                {
                    _unitSlots.FreeSpace(i);
                }
            }
        }
        
        public struct EcsWeapon
        {
            public int slot;
            public int unitSlot;
            public bool hasTurret;
            public bool homing;
            public bool drop;
            public float range;
            public EcsTurretManager.EcsTurret turret;
            public float3 offset;
            public float shellSpeed;
            public Quaternion defaultRotation;
            public float2 xLimits;
            public float2 yLimits;
        }

        private FreeArraySpaceTracker _unitSlots;
    }
}