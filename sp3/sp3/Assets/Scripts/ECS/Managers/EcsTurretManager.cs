﻿using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using EntityArchetype = Unity.Entities.EntityArchetype;

namespace ECS.Managers
{
    public class EcsTurretManager
    {
        public void AddTurret(EcsWeaponManager.EcsWeapon ecsWeapon)
        {
            var entityManager = EcsManager.entityManager;
            var renderMesh = new RenderMesh
            {
                mesh = ecsWeapon.turret.mesh,
                material = ecsWeapon.turret.material
            };
            EntityArchetype entityArchetype = entityManager.CreateArchetype(
                typeof(Translation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Scale),
                typeof(Rotation),
                typeof(RenderBounds),
                typeof(EcsUnitItem),
                typeof(EcsTurretComponent)
            );
            var ecsUnitItem = new EcsUnitItem(ecsWeapon);
            var entity = entityManager.CreateEntity(entityArchetype);
            entityManager.SetSharedComponentData(entity, renderMesh);
            entityManager.AddComponentData(entity, new Rotation {Value = ecsWeapon.turret.ecsTurretData.defaultRotation});
            entityManager.SetComponentData(entity, new Translation {Value = new float3()});
            entityManager.SetComponentData(entity, new Scale {Value = 1});
            entityManager.SetComponentData(entity, ecsUnitItem);
            AddBarrel(ecsWeapon);
        }

        private void AddBarrel(EcsWeaponManager.EcsWeapon ecsWeapon)
        {
            var entityManager = EcsManager.entityManager;
            var renderMesh = new RenderMesh
            {
                mesh = ecsWeapon.turret.ecsBarrel.mesh,
                material = ecsWeapon.turret.ecsBarrel.material
            };
            EntityArchetype entityArchetype = entityManager.CreateArchetype(
                typeof(Translation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Scale),
                typeof(Rotation),
                typeof(RenderBounds),
                typeof(EcsUnitItem),
                typeof(EcsBarrelComponent)
            );
            var ecsUnitItem = new EcsUnitItem(ecsWeapon);
            var entity = entityManager.CreateEntity(entityArchetype);
            entityManager.SetSharedComponentData(entity, renderMesh);
            entityManager.AddComponentData(entity, new Rotation {Value = new quaternion()});
            entityManager.SetComponentData(entity, new Translation {Value = new float3()});
            entityManager.SetComponentData(entity, new Scale {Value = 1});
            entityManager.SetComponentData(entity, ecsUnitItem);
        }

        public struct EcsTurret
        {
            public Mesh mesh;
            public Material material;
            public EcsBarrel ecsBarrel;
            public EcsTurretData ecsTurretData; 
        }

        public struct EcsTurretData
        {
            public float rotationSpeed;
            public quaternion defaultRotation;
            public float3 barrelOffset;
            public float barrelLength;
        }
        
        public struct EcsBarrel
        {
            public Mesh mesh;
            public Material material;
        }
    }
}