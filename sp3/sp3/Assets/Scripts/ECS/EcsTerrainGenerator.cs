﻿using Global;
using Global.Other;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using Material = UnityEngine.Material;
using MeshCollider = Unity.Physics.MeshCollider;

namespace ECS
{
    public class EcsTerrainGenerator
    {
        public void GenerateTerrain(int[] triangles, Vector3[] vertices, Material material)
        {
            var mesh = new Mesh {indexFormat = IndexFormat.UInt16, vertices = vertices, triangles = triangles};
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            var trianglesConverted = Transformers.ConvertToInt3Array(triangles);
            var verticesConverted = Transformers.CopyToNativeArray(vertices);
            var entityManager = EcsManager.entityManager;
            var renderMesh = new RenderMesh
            {
                mesh = mesh,
                material = material
            };
            EntityArchetype entityArchetype = EcsManager.entityManager.CreateArchetype(
                typeof(Translation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Scale),
                typeof(Rotation),
                typeof(RenderBounds),
                typeof(PhysicsCollider),
                typeof(EcsUnitItem)
            );
            var physicsMaterial = Unity.Physics.Material.Default;
            physicsMaterial.Flags = Unity.Physics.Material.MaterialFlags.EnableCollisionEvents |
                                    Unity.Physics.Material.MaterialFlags.EnableMassFactors |
                                    Unity.Physics.Material.MaterialFlags.EnableSurfaceVelocity;
            var collider = MeshCollider.Create(
                verticesConverted,
                trianglesConverted,
                CollisionFilter.Default,
                physicsMaterial);
            var entity = entityManager.CreateEntity(entityArchetype);
            entityManager.SetSharedComponentData(entity, renderMesh);
            entityManager.SetComponentData(entity, new EcsUnitItem(EcsUnitItem.Type.Terrain));
            entityManager.SetComponentData(entity, new PhysicsCollider {Value = collider});
            entityManager.SetComponentData(entity, new Rotation {Value = quaternion.identity});
            entityManager.SetComponentData(entity, new Translation {Value = float3.zero});
            entityManager.SetComponentData(entity, new Scale {Value = 1});
            entityManager.SetComponentData(entity, new RenderBounds
            {
                Value = new AABB
                {
                    Extents = new float3
                    {
                        x = 1000,
                        y = 1,
                        z = 1000
                    }
                }
            });
        }
    }
}