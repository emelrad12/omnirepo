﻿using ECS.Jobs;
using ECS.Jobs.Datas;
using ECS.Managers;
using Unity.Entities;
using Unity.Physics;

namespace ECS
{
    public static class EcsManager
    {
        static EcsManager()
        {
            ecsUnitManager = new EcsUnitManager();
            jobManager = new JobManager();
            ecsWeaponManager = new EcsWeaponManager();
            ecsTurretManager = new EcsTurretManager();
            physicsWorld = new PhysicsWorld(0, 0, 0);
            ecsTerrainGenerator = new EcsTerrainGenerator();
        }

        public static World world;
        public static EntityManager entityManager;
        public static readonly EcsUnitManager ecsUnitManager;
        public static readonly EcsWeaponManager ecsWeaponManager;
        public static readonly JobManager jobManager;
        public static readonly EcsTurretManager ecsTurretManager;
        public static PhysicsWorld physicsWorld;
        public static EcsTerrainGenerator ecsTerrainGenerator;
        public static MapNativeData mapNativeData;
        public static void Init(int size)
        {
            world = World.DefaultGameObjectInjectionWorld;
            entityManager = world.EntityManager;
            ecsUnitManager.Init(size);
            ecsWeaponManager.Init(size);
        }

        public static void Tick()
        {
            jobManager.Update();
            ecsUnitManager.Update();
            ecsWeaponManager.Update();
        }
    }
}