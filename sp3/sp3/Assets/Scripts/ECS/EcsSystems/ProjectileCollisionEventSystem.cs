﻿using System;
using ECS.Jobs.Datas;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;
using UnityEngine;

namespace ECS.EcsSystems
{
    [UpdateAfter(typeof(EndFramePhysicsSystem))]
    public class ProjectileCollisionEventSystem : JobComponentSystem
    {
        private BuildPhysicsWorld _buildPhysicsWorldSystem;
        private StepPhysicsWorld _stepPhysicsWorld;

        protected override void OnCreate()
        {
            _buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
            _stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        }

        [BurstCompile]
        struct CollisionEventSystemJob : ICollisionEventsJob
        {
            public ComponentDataFromEntity<EcsUnitItem> data;
            public ProjectileNativeData projectileNativeData;
            public UnitNativeData unitNativeData;

            public void Execute(CollisionEvent collisionEvent)
            {
                // var collisionPoint = collisionEvent.CalculateDetails(ref EcsManager.physicsWorld).AverageContactPointPosition;
                EcsUnitItem dataA = data[collisionEvent.Entities.EntityA];
                EcsUnitItem dataB = data[collisionEvent.Entities.EntityB];
                if (dataA.type == EcsUnitItem.Type.Projectile && dataB.type == EcsUnitItem.Type.Projectile)
                {
                    throw new Exception("Projectiles cannot collide for now");
                }

                CollisionType collisionType = CollisionType.Other;
                EcsUnitItem unit = new EcsUnitItem();
                EcsUnitItem projectile = new EcsUnitItem();

                if (dataA.type == EcsUnitItem.Type.Unit && dataB.type == EcsUnitItem.Type.Projectile)
                {
                    unit = dataA;
                    projectile = dataB;
                    collisionType = CollisionType.Projectile;
                }

                if (dataB.type == EcsUnitItem.Type.Unit && dataA.type == EcsUnitItem.Type.Projectile)
                {
                    unit = dataB;
                    projectile = dataA;
                    collisionType = CollisionType.Projectile;
                }

                if (collisionType == CollisionType.Projectile)
                {
                    if (IsNotProjectileToCollect(projectile.slot) && IsProjectileAlive(projectile.slot) && TryDamageUnit(unit.slot, projectile.slot))
                    {
                        KillProjectile(projectile.slot);
                    }
                }
            }

            private bool IsProjectileAlive(int slot)
            {
                return projectileNativeData.isAlive[slot];
            }

            private bool IsNotProjectileToCollect(int slot)
            {
                return !projectileNativeData.toCollect[slot];
            }

            private void KillProjectile(int slot)
            {
                projectileNativeData.toCollect[slot] = true;
            }

            private bool TryDamageUnit(int unitSlot, int projectileSlot)
            { 
                if (unitNativeData.additionalUnitDatas[unitSlot].allegiance != projectileNativeData.additionalProjectileDatas[projectileSlot].allegiance)
                {
                    unitNativeData.health[unitSlot] -= projectileNativeData.additionalProjectileDatas[projectileSlot].damage;
                    return true;
                }

                return false;
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            var job = new CollisionEventSystemJob
            {
                data = GetComponentDataFromEntity<EcsUnitItem>(),
                unitNativeData = EcsManager.ecsUnitManager.unitNativeData,
                projectileNativeData = EcsManager.ecsWeaponManager.projectileManager.projectileNativeData
            }.Schedule(_stepPhysicsWorld.Simulation, ref _buildPhysicsWorldSystem.PhysicsWorld, inputDependencies);
            return job;
        }

        public enum CollisionType
        {
            Projectile,
            Other
        }
    }
}