﻿using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace ECS.EcsSystems
{
    public class TurretUpdater : SystemBase
    {
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var postUpdateCommands = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();
            var weaponNativeData = EcsManager.ecsWeaponManager.weaponNativeData;
            Entities
                .WithBurst(FloatMode.Fast)
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref EcsUnitItem ecsUnitData, ref EcsTurretComponent component, ref Rotation rotation) =>
                    {
                        translation.Value = weaponNativeData.positions[ecsUnitData.slot];
                        rotation.Value = weaponNativeData.turretRotations[ecsUnitData.slot];
                        if (!weaponNativeData.isAlive[ecsUnitData.slot])
                        {
                            postUpdateCommands.DestroyEntity(entityInQueryIndex, entity);
                        }
                    }
                )
                .ScheduleParallel();
            Entities
                .WithBurst(FloatMode.Fast)
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref EcsUnitItem ecsUnitData, ref EcsBarrelComponent component, ref Rotation rotation) =>
                    {
                        translation.Value = weaponNativeData.barrelPositions[ecsUnitData.slot];
                        rotation.Value = weaponNativeData.barrelRotations[ecsUnitData.slot];
                        if (!weaponNativeData.isAlive[ecsUnitData.slot])
                        {
                            postUpdateCommands.DestroyEntity(entityInQueryIndex, entity);
                        }
                    }
                )
                .ScheduleParallel();
            m_EndSimulationEcbSystem.AddJobHandleForProducer(Dependency);
        }
    }
}