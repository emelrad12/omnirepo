﻿using System.Numerics;
using Unity.Burst;
using Unity.Entities;
using Unity.Physics;
using Unity.Transforms;

namespace ECS.EcsSystems
{
    public class UpdateUnits : SystemBase
    {
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var postUpdateCommands = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();
            var unitNativeData = EcsManager.ecsUnitManager.unitNativeData;
            Entities
                .WithBurst(FloatMode.Fast, FloatPrecision.Low)
                .ForEach(
                    (Entity entity, int entityInQueryIndex, ref Translation translation, ref EcsUnitItem ecsUnitData, ref EcsUnitComponent ecsUnitComponent,
                        ref Rotation rotation) =>
                    {
                        rotation.Value = unitNativeData.rotations[ecsUnitData.slot];
                        translation.Value = unitNativeData.positions[ecsUnitData.slot];
                        if (!unitNativeData.IsAlive(ecsUnitData.slot))
                        {
                            postUpdateCommands.DestroyEntity(entityInQueryIndex, entity);
                        }
                    }
                )
                .ScheduleParallel();
            m_EndSimulationEcbSystem.AddJobHandleForProducer(Dependency);
        }
    }
}