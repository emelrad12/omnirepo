﻿using Unity.Burst;
using Unity.Entities;
using Unity.Physics;
using Unity.Transforms;

namespace ECS.EcsSystems
{
    public class UpdateProjectiles : SystemBase
    {
        EndSimulationEntityCommandBufferSystem m_EndSimulationEcbSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            m_EndSimulationEcbSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }

        protected override void OnUpdate()
        {
            var postUpdateCommands = m_EndSimulationEcbSystem.CreateCommandBuffer().ToConcurrent();
            var projectileNativeData = EcsManager.ecsWeaponManager.projectileManager.projectileNativeData;
            Entities
                .WithBurst(FloatMode.Fast)
                .ForEach((Entity entity, int entityInQueryIndex, ref Translation translation, ref EcsUnitItem ecsUnitData, ref EcsProjectileComponent ecsProjectileComponent, ref Rotation rotation,
                        ref PhysicsVelocity velocity) =>
                    {
                        if (!projectileNativeData.isAlive[ecsUnitData.slot])
                        {
                            postUpdateCommands.AddComponent<Disabled>(entityInQueryIndex, entity);
                        }

                        projectileNativeData.positions[ecsUnitData.slot] = translation.Value;
                        if (ecsProjectileComponent.homing)
                        {
                            velocity.Linear = projectileNativeData.velocities[ecsUnitData.slot];
                        }

                        rotation.Value = projectileNativeData.rotations[ecsUnitData.slot];
                    }
                )
                .ScheduleParallel();
            Entities
                .WithBurst(FloatMode.Fast)
                .WithAll<Disabled>()
                .ForEach((Entity entity, int entityInQueryIndex, ref EcsUnitItem ecsUnitData, ref EcsProjectileComponent ecsProjectileComponent) =>
                    {
                        if (projectileNativeData.isAlive[ecsUnitData.slot])
                        {
                            postUpdateCommands.RemoveComponent<Disabled>(entityInQueryIndex, entity);
                        }
                    }
                )
                .ScheduleParallel();
            m_EndSimulationEcbSystem.AddJobHandleForProducer(Dependency);
        }
    }
}