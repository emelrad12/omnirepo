﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameAI.UnitAis;
using Units.Instances;

namespace GameAI
{
    public class UnitAiManager
    {
        public Dictionary<Guid, UnitAiCore> unitAis = new Dictionary<Guid, UnitAiCore>();

        public void CreateAi(Unit unit)
        {
            unitAis.Add(unit.guid, UnitAiCore.GetAiForUnit(unit));
        }
        
        public void UpdateAi()
        {
            Parallel.ForEach(unitAis, item =>
                item.Value.UpdateUnitOrders()
            );
        }
    }
}