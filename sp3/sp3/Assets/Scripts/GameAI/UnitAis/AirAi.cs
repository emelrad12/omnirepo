﻿using System;
using Units.Utils;

namespace GameAI.UnitAis
{
    public class AirAi : UnitAiCore
    {
        public float postStrafeTimeout = 0;

        public override void PostUpdate()
        {
            switch (state)
            {
                case UnitState.Idle:
                {
                    unit.stayStill = true;
                    break;
                }
                case UnitState.MovingToEnemy:
                {
                    unit.stayStill = false;
                    if (UnitUtils.IsUnitInRange(unit, unit.target, 0.9f))
                    {
                        state = UnitState.FiringAtEnemy;
                        unit.stayStill = true;
                    }
                    else
                    {
                        unit.PathTo(unit.target.Position);
                    }

                    break;
                }
                case UnitState.FiringAtEnemy:
                {
                    unit.ShouldBroadside = PreferBroadside;
                    if (!UnitUtils.IsUnitInRange(unit, unit.target))
                    {
                        state = UnitState.MovingToEnemy;
                    }

                    break;
                }
                case UnitState.MovingToLocation:
                {
                    if (DistanceToEnemy() < (unit.Velocity * 10).magnitude)
                    {
                        MoveToward(unit.Velocity * 50 + unit.Position);
                    }

                    unit.stayStill = false;
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }
            postStrafeTimeout--;
            if (postStrafeTimeout < 0)
            {
                
            }
        }
    }
}