﻿using System;
using Global.Templates;
using Units.Instances;
using Units.Utils;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace GameAI.UnitAis
{
    public enum UnitState
    {
        Idle,
        MovingToEnemy,
        FiringAtEnemy,
        MovingToLocation
    }

    public abstract class UnitAiCore
    {
        public Unit unit;
        public Vector3 targetPosition;
        public bool chaseTarget;
        public UnitState state;
        public bool PreferBroadside => unit.template.preferBroadside;

        public static UnitAiCore GetAiForUnit(Unit unit)
        {
            UnitAiCore returnUnit = new ShipAi();
            switch (unit.template.movementTemplate.type)
            {
                case MovementTemplate.MovementType.Land:
                    break;
                case MovementTemplate.MovementType.Air:
                    returnUnit = new AirAi();
                    break;
                case MovementTemplate.MovementType.Gunship:
                    break;
                case MovementTemplate.MovementType.Naval:
                    returnUnit = new ShipAi();
                    break;
                case MovementTemplate.MovementType.Amphibious:
                    break;
                case MovementTemplate.MovementType.Hover:
                    break;
                case MovementTemplate.MovementType.None:
                    break;
            }

            returnUnit.unit = unit;
            return returnUnit;
        }

        public virtual void TargetUnit(Unit target)
        {
            state = UnitState.MovingToEnemy;
            unit.TargetEnemy(target);
        }

        public virtual void MoveToward(Vector2 position)
        {
            state = UnitState.MovingToLocation;
            targetPosition = position;
        }

        public float DistanceToEnemy()
        {
            return (unit.Position - unit.target.Position).magnitude;
        }

        public float DistanceToTargetLocation()
        {
            if (state == UnitState.MovingToEnemy || state == UnitState.FiringAtEnemy)
            {
                return (unit.Position - unit.target.Position).magnitude;
            }
            else
            {
                return (unit.Position - unit.TargetLocation).magnitude;
            }
        }

        public virtual void UpdateUnitOrders()
        {
            unit.TargetLocation = targetPosition;
            switch (state)
            {
                case UnitState.Idle:
                {
                    unit.stayStill = true;
                    break;
                }
                case UnitState.MovingToEnemy:
                {
                    unit.stayStill = false;
                    if (UnitUtils.IsUnitInRange(unit, unit.target, 0.9f))
                    {
                        state = UnitState.FiringAtEnemy;
                        unit.stayStill = true;
                    }
                    else
                    {
                        unit.PathTo(unit.target.Position);
                    }

                    break;
                }
                case UnitState.FiringAtEnemy:
                {
                    unit.ShouldBroadside = PreferBroadside;
                    if (!UnitUtils.IsUnitInRange(unit, unit.target))
                    {
                        state = UnitState.MovingToEnemy;
                    }

                    break;
                }
                case UnitState.MovingToLocation:
                {
                    if (DistanceToTargetLocation() < 10)
                    {
                        state = UnitState.Idle;
                    }

                    unit.stayStill = false;
                    break;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (unit.target != null && unit.target.IsAlive) return;
            var newTarget = Managers.targetManager.GetBestTarget(unit);
            if (newTarget == null)
            {
                state = UnitState.Idle;
            }
            else
            {
                TargetUnit(newTarget);
                PostUpdate();
            }

            PostUpdate();
        }

        public virtual void PostUpdate()
        {
        }
    }
}